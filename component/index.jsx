import React, { useEffect, useState } from "react";

const View = (props) => { 
  const CPN = props.component_view
  return (
  	CPN?<CPN {...props.viewprops}/>:<div>Component is null</div>
  );
};

export default View;
