import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';


const View = ()=>{

	return (
	<div className="banner">
		<div className="child-bg">
			<div className="container">
				<div className="row">
					<div className="col-md-6" style={{textAlign: 'center'}}>
						<img data-aos="slide-right" className="app-screenshot" src="/img/app-phone-screenshot.png" />
					</div>
					<div className="col-md-6">
						<img className="logo-hdi-white" src="/img/logo-hdi-white.png" />
						<h1>{l.g('banner.title1')}</h1>
						<h2>{ReactHtmlParser(l.g('banner.title2'))}</h2>
						<p>{l.g('banner.title3')}</p>
						<p>{ReactHtmlParser(l.g('banner.title4'))}</p>
						<div className="app-download">
							<div className="btn-download-group">
								<img src="/img/GooglePlay.png" />
								<img className="mg-20" src="/img/AppleStore.png" />
							</div>
							<div className="app-dive-text">{l.g('banner.title_or')}</div>
							<div className="img-download-qr">
								<img className="qr-app" src="/img/websiteQRCode_noFrame.png" />
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	)
}

export default View;