import React, { useEffect, useState, createRef } from "react";

// Import css files
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
const products = [1,2,3,4,5]

const ProductList = () => {
  var slider = createRef()
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    autoplay: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: false,
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: false,
          slidesToShow: 1
        }
      }
    ]
  };

  return (
      <div className="products">
      <div className="pratical-01"/>
       <div className="hd-container">
       <div className="m-head-product-list">
        <h3 >Các sản phẩm khác</h3>
        <div className="sub-title">
          <p>Với mục đích mang đến trải nghiệm, quyền lợi và mức giá ưu đãi cho khách hàng,</p>
          <p>Bảo hiểm HD cung cấp đa dạng các loại bảo hiểm phù hợp với nhu cầu của khách hàng</p>
        </div>


       </div>

       <div className="group">
           <div className="list-product">
             <Slider {...settings} ref={c => (slider = c)}>
               {products.map(function(item, i){
                return <li key={i}>
                  <div className="product-item">
                    <a href=""><img src="/img/visa/i1.png" /></a>
                    <div className="ifo">
                      <a href=""><h5>Bảo hiểm sức khỏe</h5></a>
                      <a className="view-detail" href="">Xem quyền lợi</a>
                    </div>

                  </div>
                  <div className="view-dt">Xem chi tiết</div>
                </li>
              })}
               </Slider>
               


            </div>

            <div className="hd-containe xxk1">
                <div className="action-group-slide-mobile">
                  <img className="arleft" src="/img/arleft.svg" onClick={()=>slider.slickPrev()}/>
                  <img className="arright" src="/img/arright.svg" onClick={()=>slider.slickNext()}/>
                </div>
            </div>
        </div>



       </div>
      </div>
  );}
  

export default ProductList
