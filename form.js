import React, {
  useEffect,
  useState,
  createRef,
  forwardRef,
  useImperativeHandle,
} from "react";
import { Form } from "react-bootstrap";

const FormRender = forwardRef((props, ref) => {
  const [validated, setValidated] = useState(false);

  const formRef = createRef();
  useImperativeHandle(ref, () => ({
    handleSubmit() {
      const form = formRef.current;
      if (form.checkValidity() === false) {
        setValidated(true);
        return false;
      } else {
        return true;
      }
    },
  }));
  return (
    <Form
      className={`${props.extend_class}`}
      noValidate
      ref={formRef}
      validated={validated}
    >
      {props.children}
    </Form>
  );
});

export default FormRender;
