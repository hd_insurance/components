import Header from "./header.js";
import Footer from "./footer.js";

const ErrorView = ({title = "Xin lỗi!", message="Trang này không còn tồn tại."})=>{

	return (
		<div className="error_container">
			<Header/>
			<div style={{textAlign:"center"}} className="error_ctx">
				<img src="/img/game_end.gif" width="500px"/>
	            <h4 style={{textAlign:"center", paddingTop: 30}} className="page-error_title">{title}</h4>
	            <p>{message}</p>
	            <a className="error_ation-btn" href="https://hdinsurance.com.vn">Trang chủ Bảo Hiểm HD</a>
            </div>
            <Footer/>
        </div>
	)
}

export default ErrorView;