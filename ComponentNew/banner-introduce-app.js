import ReactHtmlParser, {
  processNodes,
  convertNodeToElement,
  htmlparser2,
} from "react-html-parser";

const View = (props) => {
  const { config } = props;
  return (
    <div className="banner-app-intro" style={{backgroundColor: 'white'}}>
      <div className="hd-container">
        <div className="banner">
          <div className="child-bg">
            <div className="container">
              <div className="row">
                <div className="col-md-6" style={{ textAlign: "center" }}>
                  <img
                    data-aos="slide-right"
                    className="app-screenshot"
                    src={config.app_phone_screenshot}
                  />
                </div>
                <div className="col-md-6">
                  <img className="logo-hdi-white" src={config.logo_hdi} />
                  <h1>{config.text_title_line1}</h1>
                  <h2>{ReactHtmlParser(config.text_title_line2)}</h2>
                  <p>{config.text_title_line3}</p>
                  <p>{ReactHtmlParser(config.text_title_line4)}</p>
                  <div className="app-download">
                    <div className="btn-download-group">
                      <img src={config.imageGooglePlay} />
                      <img className="mg-20" src={config.imageAppleStore} />
                    </div>
                    <div className="app-dive-text">{config.text_or}</div>
                    <div className="img-download-qr">
                      <img className="qr-app" src={config.imageQR} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default View;
