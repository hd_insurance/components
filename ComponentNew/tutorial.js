import React, { useEffect, useState, createRef } from "react";
import { Col, Row } from "react-bootstrap";


const ViewTutorial = (props) => {
  const {config} = props;
  return (
    <div className="tutorial">
      <Row className="hd-container">
        <Col md={6}>
          <h1 className="title-tutorial">{config.text_tutorial}</h1>
          <img className="img-tutorial" src={config.imageLeft} />
        </Col>
        <Col md={6}>
          <div className="tutorial-register">
            <div className="title-tutorial-register">
              <p>{config.text_tutorial_register}</p>
            </div>
            <div className="content-tutorial-register">
              {(config.listTutorial || []).map((item, index) => {
                return (
                  <p className="adv-item" key={index}>
                    <i className="fas fa-check-circle"></i>
                    <label className="adv-item-content">{item.title}</label>
                  </p>
                );
              })}
            </div>
          </div>
          <div className="tutorial-register">
            <div className="title-tutorial-register">
              <p>{config.text_tutorial_payment}</p>
            </div>
            <div className="content-tutorial-register">
              <p className="adv-item">
                <label className="adv-item-content">
                  {(config.listStep || []).map((item, index) => {
                    return (
                      <p className="adv-item" key={index}>
                        {index === 0 ? (<i className="fas fa-check-circle" style={{marginLeft: '-8px'}}></i>) : null}
                        <label className={`adv-item-content ${index !== 0 ? 'pl-2' : ''}`}>
                          {item.step}
                          <br />
                        </label>
                      </p>
                    );
                  })}
                </label>
              </p>
              <p className="adv-item">
                <i className="fas fa-check-circle"></i>
                <label className="adv-item-content">
                  {config.text_contract}
                </label>
              </p>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default ViewTutorial;
