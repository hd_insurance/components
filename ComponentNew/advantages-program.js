import React, { useEffect, useState, createRef } from "react";

const ViewAdvPro = (props) => {

  const {config} = props;

  return (
    <div style={{backgroundColor: 'white'}}>
      <div className="hd-container">
        <div className="row advantages_program">
          <div className="col-md-5" style={{ textAlign: "center" }}>
            <img
              className="img-advant"
              style={{ borderRadius: "50%" }}
              src={config.background}
            />
          </div>
          <div className="col-md-7">
            <p className="title-advant">{config.title}</p>
            <div className="content-advent">
              {(config.data || []).map((item, index) => {
                return (
                  <p className="adv-item" key={index}>
                    <i className="fas fa-shield-alt"></i>
                    <label> {item.title} </label>
                  </p>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ViewAdvPro;
