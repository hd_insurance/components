import Link from "next/link";
import { Button, Accordion, Row, Col } from "react-bootstrap";
import React, { useEffect, useState, createRef } from "react";

const View = (props) => {
  const {config} = props;
  const [active, setActive] = useState("0");
  const onSelect = (position) => {
    setActive(position);
  };
  return (
    <div className="aboutus">
      <div className="gr-bg">
        <div className="bbalcne" />
      </div>
      <div className="hd-container">
        <Row>
          <Col md={6}>
            <div className="content">
              <h3>{config.title}</h3>
              <Accordion defaultActiveKey={active} onSelect={onSelect}>
                {(config.data || []).map((item, index) => {
                  return (
                    <div
                      className={
                        active == index ? "abu-item-active" : "abu-item"
                      }
                      key={index}
                    >
                      <div>
                        <Accordion.Toggle
                          as={Button}
                          variant="link"
                          eventKey={index+""}
                        >
                          {item.title}
                          <i className="fas fa-chevron-down"></i>
                        </Accordion.Toggle>
                      </div>
                      <Accordion.Collapse eventKey={index+""}>
                        <div className="abu-body">
                          {(item.listContent || []).map((itemConent, i) => {
                            return (
                              <p className="adv-item" key={i}>
                                <i className="fas fa-check-circle"></i>
                                <label className="adv-item-content">
                                  {itemConent.content}
                                </label>
                              </p>
                            );
                          })}
                        </div>
                      </Accordion.Collapse>
                    </div>
                  );
                })}
              </Accordion>
            </div>
          </Col>
          <Col md={6}>
            <div className="aboutus-banner">
              <div className="particex" />
              <img src={config.image} className="imgbn" />
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default View;
