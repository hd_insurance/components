import React, { useEffect, useState, createRef } from "react";

// Import css files
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const ProductList = (props) => {
  const {config} = props;
  var slider = createRef()
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: config.slideShow,
    slidesToScroll: 1,
    autoplay: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: false,
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: false,
          slidesToShow: 1
        }
      }
    ]
  };

  return (
      <div className="products">
      <div className="pratical-01"/>
       <div className="hd-container">
       <div className="m-head-product-list">
        <h3>{config.text_title_line1}</h3>
        <div className="sub-title">
          <p>{config.text_title_line2}</p>
          <p>{config.text_title_line3}</p>
        </div>


       </div>

       <div className="group">
           <div className="list-product">
             <Slider {...settings} ref={c => (slider = c)}>
               {config.product.map(function(item, i){
                return <li key={i}>
                  <div className="product-item" style={{borderRadius: '12px'}}>
                    <img src={item.thumb} />
                    <div className="ifo">
                      <a href=""><h5>{item.title}</h5></a>
                      <div className="view-detail" style={{paddingBottom: '5px'}}></div>
                      {/* <a className="view-detail" href={item.benefit}>Xem quyền lợi</a> */}
                    </div>

                  </div>
                  <a href={item.link} className="kt-view-ct"><div className="view-dt">{config.text_see_detail}</div></a>
                </li>
              })}
               </Slider>
               


            </div>

            <div className="hd-containe xxk1">
                <div className="action-group-slide-mobile">
                  <img className="arleft" src="/img/arleft.svg" onClick={()=>slider.slickPrev()}/>
                  <img className="arright" src="/img/arright.svg" onClick={()=>slider.slickNext()}/>
                </div>
            </div>
        </div>



       </div>
      </div>
  );}
  

export default ProductList
