import React, { useEffect, useState, createRef } from "react";
import { Col, Row, Modal, Button } from "react-bootstrap";
import { isMobile } from "react-device-detect";
import moment from "moment";
import api from "../../services/Network";

const ViewTopBanner = (props) => {
  const { config } = props;
  const [applyDate, setApplyDate] = useState("02/09/2021");

  const getInitDataPackage = async () => {
    try {
      const data = await api.get(
        `/api/bhsl/packages/${config.org_code}?lang=` + l.getLang()
      );
      if (data[0].EXP_DATE) {
        setApplyDate(data[0].EXP_DATE);
      }
    } catch (e) {
      console.log(e);
    }
  };
  useEffect(() => {
    getInitDataPackage();
  }, []);

  const [eventTime, setEventTime] = useState(
    moment(applyDate, "DD/MM/YYYY")
  );
  var currentTime = moment(new Date().getTime());
  var diffTime = eventTime - currentTime;
  const [duration, setDuration] = useState(
    moment.duration(diffTime, "milliseconds")
  );
  const [background, setBackground] = useState(config.background);

  useEffect(() => {
    if (isMobile) {
      setBackground(config.bgMobile);
    }
  }, []);

  useEffect(() => {
    var evtTime = moment(applyDate, "DD/MM/YYYY");
    setEventTime(evtTime);
    var currentTime = moment();

    var diffDays = evtTime.diff(currentTime, "days");
    var diffHours = evtTime.diff(currentTime, "hours");
    var diffMinutes = evtTime.diff(currentTime, "minutes");

    var d = diffDays,
      h = diffHours - diffDays * 24,
      m = diffMinutes - diffHours * 60;

    d = d.length === 1 ? "0" + d : d;
    h = h.length === 1 ? "0" + h : h;
    m = m.length === 1 ? "0" + m : m;

    setDuration([d, h, m]);
  }, [applyDate]);

  return (
    <div className="head-top-banner">
      <div className="banner-container">
        <div className="hd-container">
          <div className="banner">
            <div
              className="child-banner-hdbank"
              style={{ backgroundImage: `url(${background})` }}
            >
              <div className="container content-top-banner">
                <h1>{config.title_one}</h1>
                <h1>{config.title_two}</h1>
                <p className="content-banner ckt_banner_bank">
                  {config.content}
                </p>
              </div>
            </div>
          </div>
          <div className="container form-register-bh">
            <div className="row time-container">
              <div className="col-12 col-md-3">
                <p className="text-time-dk">{config.time_register}</p>
              </div>

              <div className="col-12 col-md-6 form-time-dk">
                <div className="row">
                  <div className="col-4 col-md-4 time-border-right">
                    <p className="time-top-bannner">
                      {duration[0]} {l.g("bhsk.landing.day")}
                    </p>
                  </div>
                  <div className="col-4 col-md-4 time-border-right">
                    <p className="time-top-bannner">
                      {duration[1]} {l.g("bhsk.landing.hour")}
                    </p>
                  </div>
                  <div className="col-4 col-md-4">
                    {/*<label className="time-top-bannner">45 phút</>*/}
                    <p className="time-top-bannner">
                      {duration[2]} {l.g("bhsk.landing.mininute")}
                    </p>
                  </div>
                </div>
              </div>

              <div className="col-12 col-md-3 center-cont">
                <a href="./register-insurance.hdi" className="btn register-bh">
                  {config.register}
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ViewTopBanner;
