const Header = (props) => {
  const {config} = props;
  return (
    <div className="footer">
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <div className="row">
              <div className="col-md-4">
                <div className="logo-bounding">
                  <img src={config.logo_hdi} alt="HDI" />
                </div>
              </div>
              <div className="col-md-8">
                <h3 className="footer-h3 mt-20-responsive">
                  {config.name_company}
                </h3>
                <p className="m-item">
                  <i className="fas fa-headphones-alt"></i>
                  <a href="#">{config.hotline}</a>
                </p>
                <p className="m-item">
                  <i className="fas fa-envelope"></i>
                  <a href="#">{config.email}</a>
                </p>
                <p className="m-item">
                  <i className="fas fa-map-marker-alt"></i>{" "}
                  {config.address}
                </p>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <div className="row">
              <div className="col-md-5">
                <h3 className="footer-h3 hide-component">
                  {l.g("footer.title2")}
                </h3>
                <ul className="footer-menu hide-component">
                  <li>
                    <a href="/">{l.g("footer.menu.mn1")}</a>
                  </li>
                  <li>
                    <a href="/">{l.g("footer.menu.mn2")}</a>
                  </li>
                  <li>
                    <a href="/">{l.g("footer.menu.mn3")}</a>
                  </li>
                </ul>
              </div>
              <div className="col-md-7">
                <h3 className="footer-h3 mt-20-responsive">
                  {config.text_title3}
                </h3>
                <div className="input-email-subcriber">
                  <input
                    type="email"
                    placeholder={config.your_email}
                  />
                  <button className="button-submit">
                  {config.text_btn_send}
                  </button>
                </div>
                <h3 className="footer-h3 mt-20">{config.text_follow}</h3>
                <div className="social-list">
                  <a href="https://www.facebook.com/CongTyBaoHiemHD/">
                    <i className="fab fa-facebook-f"></i>
                  </a>
                  <a href="https://twitter.com/BaoHiemHD">
                    <i className="fab fa-twitter"></i>
                  </a>
                  <a href="https://www.youtube.com/channel/UCWGaAaE8tUp42xmOsDPuYzg">
                    <i className="fab fa-youtube"></i>
                  </a>
                  <a href="https://www.instagram.com/baohiemhd/">
                    <i className="fab fa-instagram"></i>
                  </a>
                  <a href="https://www.linkedin.com/company/hd-insurance-company-limited/">
                    <i className="fab fa-linkedin-in"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="footer-copy-right">
          <p>Copyright © 2021 all rights reserved</p>
        </div>
      </div>
    </div>
  );
};

export default Header;
