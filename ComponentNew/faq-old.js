import { Button, Accordion, Row, Col, Container } from "react-bootstrap";
import React, { useEffect, useState, createRef } from "react";

import api from "../../services/Network";

const ViewFAQ = (props) => {
  const { config } = props;
  const [active, setActive] = useState("0");
  const [listFAQ, setListFAQ] = useState([]);
  const [isShow, setIsShow] = useState(false);
  const [showMore, setShowMore] = useState(false);
  const [vlSearch, setVlSearch] = useState("");
  useEffect(() => {
    initFaq();
  }, []);
  const onSelect = (position) => {
    setActive(position);
  };
  const initFaq = async () => {
    const tp = await api.get(
      `/api/bhsk/faq/${l.getLang() || "VI"}/${config.org_code}`
    );
    const listFaq = tp;
    setListFAQ(listFaq);
    if (listFaq.length > 8) {
      setIsShow(true);
      setShowMore(true);
    }
  };
  const handleSearchFAQ = () => {
    const filter = listFAQ.filter((el) => {
      let vl = el.QUESTION_NAME;
      return vl.toString().includes(vlSearch);
    });
    setListFAQ(filter);
  };
  const onChangeVlSearch = (e) => {
    setVlSearch(e.target.value);
    if (e.target.value.length === 0) {
      initFaq();
    }
  };

  return (
    <div className="faq">
      <div className="container">
        <h1 className="title-faq">{config.text_title_faq}</h1>
        <div className="row justify-content-center">
          <div className="col-md-8">
            <div className="input-group custom-input">
              <input
                type="text"
                className="form-control"
                value={vlSearch}
                onChange={onChangeVlSearch}
                placeholder={config.placeholder_search}
                aria-describedby="inputGroupPrepend2"
              />
              <div className="input-group-prepend" onClick={handleSearchFAQ}>
                <input
                  type="submit"
                  className="btn"
                  value={config.text_btn_search}
                />
              </div>
            </div>
          </div>
        </div>
        <div className={showMore ? "row height-compact" : "row height-expand"}>
          {listFAQ
            ? listFAQ.map((value, i) => {
                return (
                  <div className="col-12 col-md-6">
                    <Accordion defaultActiveKey={active} onSelect={onSelect}>
                      <div
                        className={
                          active === `${i}`
                            ? "abu-item-active-faq"
                            : "abu-item-faq"
                        }
                      >
                        <div>
                          <Accordion.Toggle
                            as={Button}
                            variant="link"
                            eventKey={`${i}`}
                          >
                            <div className="faq-ic">
                              <i className="fas fa-question-circle"></i>
                            </div>
                            {value.QUESTION_NAME}
                            <i className="fas fa-chevron-down"></i>
                          </Accordion.Toggle>
                        </div>
                        <Accordion.Collapse eventKey={`${i}`}>
                          <div
                            className="abu-body"
                            dangerouslySetInnerHTML={{ __html: value.CONTENT }}
                          ></div>
                        </Accordion.Collapse>
                      </div>
                    </Accordion>
                  </div>
                );
              })
            : null}
        </div>
      </div>
      {isShow ? (
        <div className="btn-colapp">
          <div onClick={() => setShowMore(!showMore)}>
            <label>
              {showMore
                ? `${config.text_view_more}`
                : `${config.text_btn_compact}`}
            </label>
            <i
              className={showMore ? "fas fa-chevron-down" : "fas fa-chevron-up"}
            ></i>
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default ViewFAQ;
