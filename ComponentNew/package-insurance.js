import React, { useEffect, useState, createRef } from "react";
import api from "../../services/Network";

const ViewPackgeInsur = (props) => {
  const { config } = props;
  const [listPackage, setListPackage] = useState([]);

  const getInitDataPackage = async () => {
    try {
      const data = await api.get(
        `/api/bhsl/packages/${config.org_code}?lang=` + l.getLang()
      );
      setListPackage(data);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getInitDataPackage();
  }, []);

  // dành cho nhân viên HDBANK, HDSAISON, PHULONG
  const ComponentOne = (
    <div style={{ position: "relative" }}>
      <div className="hd-container packge-hdbank">
        <h1>{config.title_one}</h1>
        <p>{config.title_two}</p>
        <div className="content-packge-hdbank">
          {listPackage
            ? listPackage.map((vl, i) => (
                <div key={i}>
                  <div className="header-content-package">
                    <div className="name-package-insur-hdbank">
                      {config.title_three} {vl.PACK_NAME}
                    </div>
                    <div>{vl.FEES + " VNĐ/người"}</div>
                  </div>
                  <div className="benefit-package-hdbank">
                    <div className="container">
                      <div className="row">
                        {vl.BENEFITS
                          ? vl.BENEFITS.map((value, index) => (
                              <div className="col-md-6" key={index}>
                                <p className="adv-item">
                                  <i className="fas fa-check-circle"></i>
                                  <label className="adv-item-content">
                                    {value.NAME}
                                  </label>
                                </p>
                              </div>
                            ))
                          : null}
                      </div>
                    </div>
                  </div>
                  <div className="container detail-package-hdbank">
                    <div className="row">
                      <div className="col-12 col-md-6">
                        <a
                          href={
                            "./register-insurance.hdi?pack_code=" + vl.PACK_CODE
                          }
                          className="btn register-package-hdbank"
                        >
                          {l.g("saison.landing.pl_package_dk")}
                        </a>
                      </div>
                      <div className="col-12 col-md-6">
                        <p>
                          <a
                            href={vl.URL_DETAIL}
                            target="_blank"
                            className="text-detail"
                          >
                            {config.text_detail}
                          </a>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              ))
            : null}
        </div>
      </div>
      <div>
        <img className="img-background-package" src={config.image} />
      </div>
    </div>
  );

  // dành cho nhân viên VietJet
  const ComponentTwo = (
    <div className="package-insurrance">
      <h1>{l.g("bhsk.landing.vj_t_17")}</h1>
      <p>{l.g("bhsk.landing.vj_t_15")}</p>
      <p>{l.g("bhsk.landing.vj_t_18")}</p>
      <div className="hd-container packge-hdbank">
        <div className="row view-positins">
          {listPackage
            ? listPackage.map((data, index) => {
                return (
                  <div key={index} className="col scroll-item">
                    <div className="info-item-packge">
                      <div className="title-item-package">
                        <h1>{data.PACK_NAME}</h1>
                        <p>{data.FEES + " " + l.g("bhsk.currency")}</p>
                      </div>
                      <div className="benefit-item-package">
                        {data.BENEFITS
                          ? data.BENEFITS.map((value, i) => {
                              return (
                                <p key={i} className="adv-item">
                                  <i className="fas fa-check-circle"></i>
                                  <label className="adv-item-content">
                                    {value.NAME}
                                  </label>
                                </p>
                              );
                            })
                          : null}
                      </div>
                      <div className="footer-item-package">
                        <a
                          href={
                            "./register-insurance.hdi?pack_code=" +
                            data.PACK_CODE
                          }
                          className="btn register-item-package"
                        >
                          {l.g("bhsk.landing.register")}
                        </a>
                        <p>
                          <a
                            href={data.URL_DETAIL}
                            target="_blank"
                            className="detail-item-package"
                          >
                            {l.g("bhsk.form.lbll_detail")}
                          </a>
                        </p>
                      </div>
                    </div>
                  </div>
                );
              })
            : null}
        </div>
        <div>
          <img
            className="img-background-package"
            src="/img/img_backround_package_insur.png"
          />
        </div>
      </div>
    </div>
  );

  switch (config.type) {
    case "type_two": {
      return ComponentTwo; // dành cho nhân viên VietJet
    }
    default: {
      return ComponentOne; // dành cho nhân viên HDBANK, HDSAISON, PHULONG
    }
  }
};

export default ViewPackgeInsur;
