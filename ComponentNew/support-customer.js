import React, { useEffect, useState, createRef } from "react";
import {Col, Row} from "react-bootstrap";
const ViewSupportCustomer = (props) => {
    const {config} = props;
    return(
        <div className="support-kh">
            <div className="hd-container">
                <div className="background-support">
                    <h1>{config.text_title_line1}</h1>
                    <p>{config.text_line2}</p>
                    <p>{config.text_line3}</p>
                    <Row className="contact-info">
                        <Col md={4}>
                            <p><i className="fas fa-phone-alt"></i> <a>{config.hotline}</a></p>
                        </Col>
                        <Col md={4}>
                            <p><i className="fas fa-envelope-open"></i> <a>{config.email}</a></p>
                        </Col>
                        <Col md={4}>
                            <p><i className="fas fa-globe-americas"></i> <a>{config.link}</a></p>
                        </Col>
                    </Row>
                </div>
            </div>
            <div className="bg-sp">
                <img className="img-bg-sp" src={config.imageRight} />
            </div>
        </div>
    )};

export default ViewSupportCustomer;
