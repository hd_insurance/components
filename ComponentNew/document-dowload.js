import React, { useEffect, useState, createRef } from "react";
import {Col, Row} from "react-bootstrap";
import api from "../../services/Network";

const ViewDocumentDown = (props) => {

  const [listPackage, setListPackage] = useState([]);
  const {config} = props;
  const [docs, setDocs] = useState({})

  const getInitDataPackage = async () => {
    try {
      const data = await api.get(
        `/api/bhsl/packages/${config.org_code}?lang=` + l.getLang()
      );
      if(data.length > 0){
        const doc = {
            URL_BENE: data[0].URL_BENE,
            URL_RULE: data[0].URL_RULE,
            URL_GYC: data[0].URL_GYC,
            URL_BV: data[0].URL_BV,
            URL_BT: data[0].URL_BT
        }
        setDocs(doc)
      }
      setListPackage(data);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getInitDataPackage();
  }, []);

    return(
        <div className="document-down">
            <h1>{config.title}</h1>
            <div className="hd-container view-positins">
                <Row style={{justifyContent: 'center'}}>
                    <Col md={4}>
                        <div className="item-document">
                            <i className="fas fa-file-alt"></i>
                            {config.text_docs1}
                           <a target="_blank" href={`https://hyperservices.hdinsurance.com.vn${docs.URL_BENE}?download=true`}><label>({config.text_download})</label></a>
                        </div>
                    </Col>
                    <Col md={4}>
                        <div className="item-document">
                            <i className="fas fa-file-alt"></i>
                            {config.text_docs5}
                            <a target="_blank" href={`https://hyperservices.hdinsurance.com.vn${docs.URL_RULE}?download=true`}><label>({config.text_download})</label></a>
                        </div>
                    </Col>
                    <Col md={4}>
                        <div className="item-document">
                            <i className="fas fa-file-alt"></i>
                            {config.text_docs2}
                            <a target="_blank" href={`https://hyperservices.hdinsurance.com.vn${docs.URL_GYC}?download=true`}><label>({config.text_download})</label></a>
                        </div>
                    </Col>
                </Row>
                <Row style={{justifyContent: 'center',}}>
                    <Col md={4}>
                        <div className="item-document">
                            <i className="fas fa-file-alt"></i>
                            {config.text_docs3}
                            <a target="_blank" href={`https://hyperservices.hdinsurance.com.vn${docs.URL_BV}?download=true`}><label>({config.text_download})</label></a>
                        </div>
                    </Col>
                    <Col md={4}>
                        <div className="item-document">
                            <i className="fas fa-file-alt"></i>
                            {config.text_docs4}
                            <a target="_blank" href={`https://hyperservices.hdinsurance.com.vn${docs.URL_BT}?download=true`}><label>({config.text_download})</label></a>
                        </div>
                    </Col>
                </Row>
            </div>
            <img className="img-bg-dc" src={config.image_bg} />
        </div>
    )};

export default ViewDocumentDown;
