
import Header from './header';
import Footer from './footer';
const Layout = props => (
    <div>
        <Header config={props.headerconfig} org={props.org} />
        {props.children}
        <Footer/>
    </div>
);

export default Layout;
