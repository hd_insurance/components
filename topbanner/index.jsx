import React, { useEffect, useState } from "react";
const View = (props) => { 
  return (
    <div className={`top-head-banner ${props.className}`}>
       {props.children}
    </div>
  );
};

export default View;
