import React, { useEffect, useState } from "react";
import router from "next/router";
import dynamic from 'next/dynamic'
import { Container, Button } from "react-bootstrap";
import {isMobile} from 'react-device-detect';
import FormBH from "../../../landing_modules/module_vehicle/tnds";
import Utils from '../../../services/utils';

const FormSDK = React.forwardRef((props, ref) => {
  const { background, type, sdk_id, view, config, link_mobile, show_mobile, config_id } = props.config;
  const {ORG_CODE} = props?.configProduct;
  const [DynamicComponentWithNoSSR, setDynamicComponentWithNoSSR] = useState(false);
  const [sdk_cfg, setSDKcfg] = useState(false);

 // console.log('show_mobile', show_mobile);

  const [source, setSource] = useState("");
  const [formSDK, setFormSDK] = useState(null);


  const getFormLayout = async ()=>{
     const language = getCookie("current_language") || "vi"
     const form_sdk = await Utils.getFormSDK(sdk_id, true, language)
     if(form_sdk){
       const product_sdk_config = {...form_sdk.form_config, ...config}

       console.log("DKM >> ",form_sdk.form_config, config)
       setSDKcfg(product_sdk_config)
       const view_layout = import('../../../sdk/src/sdk_modules/'+form_sdk.file_path)
        var NoSSR = dynamic(
          () => view_layout,
          { ssr: false}
        )
        setDynamicComponentWithNoSSR(NoSSR)
      setFormSDK(form_sdk)
     }
  }

const getCookie = (key) => {
  var b = document.cookie.match("(^|;)\\s*" + key + "\\s*=\\s*([^;]+)");
  return b ? b.pop() : "";
}
  
  useEffect(() => {
    setSource(router.router.query ? router.router.query.source : "");
    getFormLayout()
  }, []);

  return (
    <div
      className={
        type === "tnds" ? "contain_form contain_form_tnds_kt" : "contain_form"
      }
    >
        {isMobile && !show_mobile ? null : (
            <div
                className={
                    type === "tnds"
                        ? "contain_bg_form contain_bg_form_tnds_kt"
                        : "contain_bg_form contain_bg_form_other"
                }
                style={{ backgroundImage: `url(${background})`}}
            >
                <div className="title_buy_isr" ref={ref}>
                    <span>Mua bảo hiểm</span>
                </div>
                {/*<div className="contain_line_buy_isr">*/}
                {/*    <div className="line_buy_isr" />*/}
                {/*</div>*/}
                <Container
                    className="custom_container hide_kitin_component"
                    style={{ maxWidth: "1422px" }}
                >
                    {formSDK?<DynamicComponentWithNoSSR
                        page_layout={formSDK.layout_component}
                        productConfig={sdk_cfg}
                    />:<div></div>}

                </Container>
                <div className="show_kitin_component">
                    <div className="content_text_buy_hqk">
                        <p>
                            Để tiến hành mua bảo hiểm, vui lòng ấn vào nút{" "}
                            <span className="hight_light_kt">“Mua bảo hiểm”</span> ở phía
                            dưới.
                        </p>
                        <Button className="hqk_isr_btn_buy"><a href={link_mobile} target="_blank">Mua Bảo Hiểm</a></Button>
                    </div>
                </div>
            </div>
        )}
    </div>
  );
});

export default FormSDK;
