import React, { useEffect, useState } from "react";
import { Container, Button, Form } from "react-bootstrap";
import { FLInput, FLSelect } from "hdi-uikit";
import cogoToast from "cogo-toast";
import api from "../../../services/Network.js";
import {MetroSpinner} from "react-spinners-kit";

const Introduce = (props) => {
  const { background, image, type, title, content } = props.config;
 
  const FormContact = () => {
    const [fullname, setFullName] = useState("");
    const [gender, setGender] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");
    const [validated, setValidated] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
  
    const handleSubmit = async (event) => {
      const form = event.currentTarget;
      event.preventDefault();
      event.stopPropagation();
      if (form.checkValidity() === false) { 
        setValidated(true);
        return;
      }
      try {
        setIsLoading(true);
        let data = {
          gender,
          phone,
          email,
          name: fullname,
        }
        const response = await api.post(`/api/request-contact/submit`, data);
        if(response.success){
          setValidated(false);
          setTimeout(() => {
            setEmail('');
            setGender('');
            setPhone('');
            setFullName('');
            setIsLoading(false);
            cogoToast.success('Gửi Thông Tin Thành Công');
          }, 1000);
        } else {
          setIsLoading(false);
          cogoToast.error('Có lỗi xảy ra');
        }
      } catch(err) {
        setIsLoading(false);
        console.log('err', err);
      }
    }

    return (<Form
      noValidate
      validated={validated}
      onSubmit={handleSubmit}
    >
      <div className="row">
        <div className="col-6">
          <FLSelect
            disable={false}
            label={"Danh xưng"}
            dropdown={true}
            required={true}
            value={gender}
            changeEvent={setGender}
            data={[
              { label: "Ông (Mr)", value: "M" },
              { label: "Bà (Mrs)", value: "F" },
            ]}
          />
        </div>
        <div className="col-6">
          <FLInput 
            label={"Họ Tên"} 
            required={true} 
            value={fullname}
            changeEvent={setFullName}
          />
        </div>
      </div>
      <div className="row mt-3">
        <div className="col-6">
          <FLInput 
            label={"Số điện thoại"} 
            required={true} 
            pattern={"[0-9]{9,12}"} 
            value={phone}
            changeEvent={setPhone}
          />
        </div>
        <div className="col-6">
          <FLInput 
            label={"Email"} 
            required={true} 
            pattern={"[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"} 
            value={email}
            changeEvent={setEmail}
          />
        </div>
      </div>
      <div className="row mt-3">
        <div className="col-md-6 cjufe_btn_hu">
          <Button className="btn_submit_kt" type="submit" > 
            <MetroSpinner size={25} color="white" loading={isLoading} /> 
            {isLoading ? '' : 'Gửi Thông Tin'}
          </Button>
        </div>
      </div>
    </Form>)
  };

  const ComponentTypeTnds = (
    <div
      className="contain_bg_contact"
      style={{ backgroundImage: `url(${background})` }}
    >
      <Container className="custom_container ctn_contact_hqk">
        <div className="col-md-4 p-0">
          <div className="title_contact">
            <h2>Bạn đang cần tư vấn ?</h2>
          </div>
          <div>
            <div className="line_contact" />
          </div>
        </div>
        <div className="col-md-8 p-0">
          <div className="form_contact_kt">
            <div className="container">
              <div className="row content_form_contact">
                Hãy để lại thông tin và đội ngũ CSKH của Bảo hiểm HD sẽ liên lạc
                để hỗ trợ, tư vấn
              </div>
              <FormContact />
            </div>
          </div>
        </div>
      </Container>
    </div>
  );

  const ComponentTypeTncn = (
    <div
      className="contain_bg_contact custom_height_tncn"
      style={{ backgroundImage: `url(${background})` }}
    >
      <Container className="custom_container ctn_contact_hqk">
        <div className="col-md-7 p-0 custom_padding_tncn">
          <div className="ctn_contact_title">
            <h2>Bạn đang cần tư vấn ?</h2>
          </div>
          <div className="contain_line_contact_tncn">
            <div className="line_contact" />
          </div>
          <div className="container p-0">
            <div className="row content_detail_tncn m-0">
              Hãy để lại thông tin và đội ngũ CSKH của Bảo hiểm HD sẽ liên lạc
              để hỗ trợ, tư vấn
            </div>
            <FormContact />
          </div>
        </div>
        <div className="col-md-5">
          <div className="row tncn_img_right_detail">
            <img src={image} alt="" />
          </div>
        </div>
      </Container>
    </div>
  );

  const ComponentTypeTcb = (
    <div className="contain_tcb_contact">
      <div className="contain_tcb_contact_detail">
        <Container className="custom_container ctn_contact_hqk">
          <div className="col-md-5">
            <div className="row tcb_img_left">
              <img src={image} alt="" />
              <div className="box_tcb_ccc" />
            </div>
          </div>
          <div className="col-md-7 p-0 cxx_tcb_padding_top">
            <div className="ctn_contact_title cxx_tcb_padding">
              <h2>Bạn đang cần tư vấn ?</h2>
            </div>
            <div className="contain_line_contact_tncn">
              <div className="line_contact" />
            </div>
            <div className="container p-0">
              <div className="row content_detail_tncn m-0">
                Hãy để lại thông tin và đội ngũ CSKH của Bảo hiểm HD sẽ liên lạc
                để hỗ trợ, tư vấn
              </div>
              <FormContact />
            </div>
          </div>
        </Container>
      </div>
    </div>
  );

  const ComponentTypeEclaim = (
    <div className="contain_tcb_contact">
      <div className="contain_tcb_contact_detail">
        <Container className="custom_container ctn_contact_hqk">
          <div className="col-md-5">
            <div className="row tcb_img_left">
              <img src={image} alt="" />
              <div className="box_tcb_ccc" />
            </div>
          </div>
          <div className="col-md-7 p-0 cxx_tcb_padding_top">
            <div className="ctn_contact_title cxx_tcb_padding">
              <h2>Bạn đang cần tư vấn ?</h2>
            </div>
            <div className="contain_line_contact_tncn">
              <div className="line_contact" />
            </div>
            <div className="container p-0">
              <div className="row content_detail_tncn m-0">
                Hãy để lại thông tin và đội ngũ CSKH của Bảo hiểm HD sẽ liên lạc
                để hỗ trợ, tư vấn
              </div>
              <FormContact />
            </div>
          </div>
        </Container>
      </div>
    </div>
  );

  const ComponentTypeVNAT = (
      <div className="contain_tcb_contact">
        <div className="contain_tcb_contact_detail">
          <Container className="custom_container ctn_contact_hqk">
            <div className="col-md-5">
              <div className="row tcb_img_left">
                <img src={image} alt="" />
                <div className="box_tcb_ccc" />
              </div>
            </div>
            <div className="col-md-7 p-0 cxx_tcb_padding_top">
              <div className="ctn_contact_title_vnat cxx_tcb_padding">
                <span>{title}</span>
              </div>
              <div className="container p-0">
                <div className="row content_detail_vnat m-0">
                  {content}
                </div>
                <FormContact />
              </div>
            </div>
          </Container>
        </div>
      </div>
  )




  switch (type) {
    case "tnds": {
      return ComponentTypeTnds;
    }
    case "tncn": {
      return ComponentTypeTncn;
    }
    case "tcb": {
      return ComponentTypeTcb;
    }
    case "ntn": {
      return ComponentTypeTncn;
    }
    case "eclaim": {
      return ComponentTypeEclaim;
    }
    case "vnat": {
      return ComponentTypeVNAT;
    }
    default: {
      <div>Component Not Found</div>;
    }
  }
};

export default Introduce;
