import React, { useEffect, useState, createRef, forwardRef } from "react";
import QnA from "./qna.js";
import { Container } from "react-bootstrap";

const View = forwardRef((props, ref) => {
  return (
    <div className="container_qna_kt">
        {props?.config?.type === 'vnat' ? (
            <>
                <div className="title_qna_vnat">
                    <span>{props?.config?.title}</span>
                </div>
                <p className="sub_title_qna">{props?.config?.content}</p>
            </>
        ) : (
            <>
                <div className="title_qna">
                    <h2>{props?.config?.title}</h2>
                </div>
                <div className="contain_line_qna">
                    <div className="line_qna_kt" />
                </div>
            </>
        )}

      <Container className="custom_container p-0 reponsible_qna_mobile">
        <QnA idQna={props.config.id} />
      </Container>
    </div>
  );
});

export default View;
