import React, {
  useEffect,
  useState,
  useContext,
  forwardRef,
  createRef,
} from "react";
import ReactHtmlParser from "react-html-parser";
import {
  Tab,
  Nav,
  Accordion,
  AccordionContext,
  useAccordionToggle,
} from "react-bootstrap";

import api from "../../../services/Network.js";
import globalStore from "../../../services/globalStore";

const View = forwardRef((props, ref) => {

  const [faqdata, setFAQData] = useState([]);
  const [activeTab, setActiveTab] = useState(
      `tab_${props.activeTab ? props.activeTab : "0"}`
  );
  const [isShowMore, setIsShowMore] = useState(false);

  useEffect(() =>{
    getInitFAQ(props.idQna);
  },[props.idQna]);


  useEffect(() => {
    if (props.activeTab) {
      setActiveTab(`tab_${props.activeTab}`);
    }
  }, [props.activeTab]);

  const ContextAwareToggle = ({ children, eventKey, callback }) => {
    const currentEventKey = useContext(AccordionContext);

    const decoratedOnClick = useAccordionToggle(
        eventKey,
        () => callback && callback(eventKey)
    );

    const isCurrentEventKey = currentEventKey === eventKey;

    return (
        <div
            className={isCurrentEventKey ? "qna-item-cl active" : "qna-item-cl"}
            onClick={decoratedOnClick}
        >
          <div className="title-item-clapse">{children}</div>

          <div className="cl-item-icon">
            {isCurrentEventKey ? (
                <i className="fas fa-minus-circle"></i>
            ) : (
                <i className="fas fa-plus-circle"></i>
            )}
          </div>
        </div>
    );
  };

  const getInitFAQ = async (id) => {
    try {
      const faqs = await api.get(
          `/api/faq/${id}?version=${globalStore.get("page_version")}`
      );

      // console.log("faqs ", faqs)
      setFAQData(faqs.data);
    } catch (e) {}
  };

  const replaceText = (rg, rg2) => {
    return lng.get(rg2);
  };

  const getTextFromCode = (intext) => {
    if (intext) {
      const regex_rs = intext.replace(/<lang>(.*?)<\/lang>/g, replaceText);
      if (regex_rs) {
        return regex_rs;
      }
    }

    return intext;
  };
  const handleSelectTab = (activeKey) => {
    setActiveTab(activeKey);
  };

  return (
      <div className="qna-container">
        <div className="top-container hd-container">
          <h1 className="uppercase-first-letter">{props.title}</h1>
        </div>
        <div className="hd-container">
          <Tab.Container
              id="qna-tabs"
              className="qna-tabs"
              onSelect={handleSelectTab}
              activeKey={activeTab}
          >
            <div className="row">
              <div className="col-md-3 qna-tabs">
                <Nav variant="pills" className="row">
                  {faqdata.map((item, index) => {
                    return (
                        <Nav.Item
                            key={index}
                            className="tab-title-item col-md-12 col-6"
                        >
                          <Nav.Link eventKey={`tab_${index}`}>
                            <div className="icon">
                              <i className={item.icon}></i>
                            </div>{" "}
                            <span>{item.title}</span>
                          </Nav.Link>
                        </Nav.Item>
                    );
                  })}
                </Nav>
              </div>
              <div className="col-md-9">
                <Tab.Content>
                  {faqdata.map((item, index) => {
                    return (
                        <Tab.Pane eventKey={`tab_${index}`} key={index}>
                          <Accordion
                              bsPrefix={`accordion ${
                                  item.content.length > 5
                                      ? isShowMore
                                      ? "accordion-faq-show"
                                      : "accordion-faq"
                                      : ""
                              }`}
                              defaultActiveKey={`item_${index}_0`}
                          >
                            {item.content.map((child_item, child_index) => {
                              return (
                                  <div key={child_index} className="ac-qna">
                                    <ContextAwareToggle
                                        eventKey={`item_${index}_${child_index}`}
                                    >
                                      {child_item.title}
                                    </ContextAwareToggle>

                                    <Accordion.Collapse
                                        eventKey={`item_${index}_${child_index}`}
                                    >
                                      <div className="qna-content-item">
                                        <div>
                                          {ReactHtmlParser(
                                             child_item.content
                                          )}
                                        </div>
                                      </div>
                                    </Accordion.Collapse>
                                  </div>
                              );
                            })}
                          </Accordion>
                          {item.content.length > 5 ? (
                              <div
                                  className="d-flex justify-content-center show-more-btn"
                                  onClick={() => setIsShowMore(!isShowMore)}
                              >
                                <div>
                                  {isShowMore ? (
                                      <div>
                                        <label>{lng.get("hofhw3zh")}</label>
                                        <i className="fas fa-chevron-up"></i>
                                      </div>
                                  ) : (
                                      <div>
                                        <label>{lng.get("gb61crtu")}</label>
                                        <i className="fas fa-chevron-down"></i>
                                      </div>
                                  )}
                                </div>
                              </div>
                          ) : null}
                        </Tab.Pane>
                    );
                  })}
                </Tab.Content>
              </div>
            </div>
          </Tab.Container>
        </div>
      </div>
  );
});

export default View;
