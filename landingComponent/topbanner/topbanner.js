import React, { useEffect, useState } from "react";
import TopBanner from "../../topbanner";
import Tilt from "react-tilt";
import { Container, Button } from "react-bootstrap";
import {isMobile} from 'react-device-detect';

const View = (props) => {
  const {nameBanner, nameIsr, slogan, imgRight, background, type } = props.config;

  const handleRegister = () => {
    if(isMobile){
      window.location.href = 'https://google.com'; 
      return;
    }
    props.scrollForm();
  }

  return (
    <div>
      <TopBanner className="custom_banner_top">
        <div
          className="topbanner-gb"
          style={{ backgroundImage: `url(${background})` }}
        >
          <Container className="custom_container">
            
            {type=="style1"?<div>
              

              <div className="row banner_detail_kt m-0 banner-style-1">
                <div className="col-md-6 p-0 full-h">
                  <div className="text-slogan" dangerouslySetInnerHTML={{ __html: slogan }}/>
                </div>
                <div className="col-md-6 p-0"></div>
              </div>


            </div>:<div className="row banner_detail_kt m-0">
              <div className="col-md-6 p-0">
                <div className="ktisr_banner">
                  {nameBanner ? <h3 className="name_banner_vnat">{nameBanner}</h3> : <h3 lng="0ecdhgrm">Bảo Hiểm </h3>}
                  {/*<h3 lng="0ecdhgrm">Bảo Hiểm </h3>*/}
                  <h1 lng="tibpt6yf">{nameIsr}</h1>
                  <p lng="xfzregcg">{slogan}</p>
                  {/* <div className="form-search-visa">
                    <div className="row" style={{ width: "100%" }}>
                      <div className="col-6 contain_btn_resgiter">
                        <Button className="tnds_btn_resgiter" onClick={handleRegister}>Đăng kí</Button>
                      </div>
                      <div className="col-6">
                        <Button className="tnds_btn_share">Chia sẻ ngay</Button>
                      </div>
                    </div>
                  </div> */}
                </div>
              </div>

              <div className="col-md-6 p-0">
                <Tilt
                  className="Tilt"
                  options={{ reverse: true, max: 25, scale: 1.1 }}
                >
                  <div className="divpcard custom_img_banner">
                    <img
                      className={type === 'tcb' ? 'divcard pb-4' : 'divcard'}
                      src={imgRight}
                    />
                  </div>
                </Tilt>
              </div>
            </div>}
            


          </Container>
        </div>
      </TopBanner>
    </div>
  );
};

export default View;
