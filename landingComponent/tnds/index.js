import React, {useEffect, useState} from "react";
import {Container} from "react-bootstrap";
import Popover from "react-popover";
import Slider from "react-slick";
import ReactHtmlParser from 'react-html-parser';
import api from "sdk-hdi/src/services/Network";
import {isMobile} from 'react-device-detect';

import util from "../../render/util";

const PopoverItem = ({id, item}) => {
  const [popoverOpen, setPopoverOpen] = useState(false);
  const toggle = () => setPopoverOpen(!popoverOpen);
  return (
    <Popover 
      isOpen={popoverOpen}
      place={"below"}
      preferPlace={"right"}
      onOuterAction={() => toggle()}
      body={[
        <div className="contain_detail_ql">
          <Container>
            <div className="ct_titile_ql">
              <p className="m-0">Quyền Lợi</p>
            </div>
            <div className="pt-2 strong_title_ql">
              {item.content}
            </div>
            <div className="content_detail_hh">
              {
                item.contentDetail ? ReactHtmlParser(item.contentDetail) : null
              }
            </div>
          </Container>
        </div>,
      ]}
      target={"Popover-" + id}
    >
      <div className="btn_view_detail" onClick={() => toggle()}  id={"Popover-" + id} >
        <a>Xem chi tiết{" "}
          <span>
            <i className="fas fa-arrow-right"></i>
          </span>
        </a>
      </div>
    </Popover>
  );
}

const TndsInfomation = (props) => {
  const { background, type, data, title, content, link_mobile, hideRegister } = props.config;
  const {org_code,product_code, channel, category, sku } = props.configProduct;
  const [bgBenefit, setBgBenefit] = useState({});
  const [open, setOpen] = useState(false);
  const [infoPack, setInfoPack] = useState([]);

  const getLangCode = () => {
    if (lng) {
      return lng.getLang() === "vi" ? "VN" : "EN";
    }
    return "VN";
  };

  useEffect(() =>{
    getBenefitPack();
  },[])


  useEffect(() => {
    if (type === "tncn" || type === 'ntn' || type === 'vnat') {
      setBgBenefit({ backgroundImage: `url(${background})` });
    } else {
      setBgBenefit({ background: "transparent" });
    }
  }, [type]);

  const handleRegister = () => {
    props.scrollForm();
  }

  const getBenefitPack = async () => {
    try {
      // landing sku
      const data = await  api.get(`/api/sdk-form/define/${org_code}/${product_code}/${channel}/${getLangCode()}?sku=${sku || ''}`);
      if (data.data) {
        setInfoPack(data?.data[2]);
      } else {
      }
      // setPackages(data);
    } catch (e) {
      console.log(e);
    }
  };

  const SampleNextArrow = (props) => {
    const { className, style, onClick } = props;
    return (
        <div
            className={`${className} custom_arrow_left`}
            style={{ ...style }}
            onClick={onClick}
        >
          <img src="/img/landing/left-arrow.png" alt="left_arrow" />
        </div>
    );
  };

  const SamplePrevArrow = (props) => {
    const { className, style, onClick } = props;
    return (
        <div
            className={`${className} custom_arrow_right`}
            style={{ ...style }}
            onClick={onClick}
        >
          <img src="/img/landing/right-arrow.png" alt="right_arrow" />
        </div>
    );
  };

  const settings = {
    dots: false,
    infinite: true,
    // centerMode: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    className: "custom-slider",
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  const ComponentDefaut = (
    <div
      style={bgBenefit}
      className={
        type === "tnds"
          ? "ctn_benefit_ldhk ctn_benefit_tnds"
          : "ctn_benefit_ldhk"
      }
    >
      <Container className="custom_container">
        <div className="contain_tnds_infor">
          <div className={`row title_tnds_infor m-0 ${['tncn', 'ntn'].includes(type) ? "title_tnds_text_start" : ""}`}>
            <h2>{title}</h2>
          </div>
          <div className={`row title_tnds_infor m-0 ${['tncn', 'ntn'].includes(type) ? "title_tnds_text_start" : ""}`}>
            <div className="line_tnds_infor" />
          </div>
          <div className="row ctn_infor_detail m-0">
            {(data || []).map((item, index) => {
              return (
                 // <div key={index} className={`package_ctn_infor`} style={{width: `calc((100% - (25px*${data.length}))/${data.length})`}}>
                <div key={index} className={`package_ctn_infor ${type === 'ntn' ? 'style_ntn_kk' : ''}`}>
                  <div className="kt_pkg_detail">
                    <div className={ ['tncn', 'ntn'].includes(type) ? "ctn_contain_icon benefit_bg_icon_tncn" : "ctn_contain_icon benefit_bg_icon"}>
                      <img src={item.icon} alt="group_user" />
                    </div>
                    <div className="content_pgk_detail">
                      <p>{item.content}</p>
                      <PopoverItem id={index} item={item} />
                      {/* <Popover 
                        isOpen={open}
                        place={props.place || "below"}
                        preferPlace={"right"}
                        onOuterAction={() => setOpen(false)}
                        body={[
                          <div>{item.content}</div>,
                        ]}
                        target={"Popover-" + index}
                      >
                        <div className="btn_view_detail" onClick={() => setOpen(true)}  id={"Popover-" + index} >
                          <a>
                            Xem chi tiết{" "}
                            <span>
                              <i className="fas fa-arrow-right"></i>
                            </span>
                          </a>
                        </div>
                      </Popover> */}
                    </div>
                  </div>
                </div>
                
              );
            })}
          </div>
        </div>
      </Container>
    </div>
  );
  const ComponentTCB = (
    <div className="contain_cpn_ctb">
      <div className="contain_benefit_ctb_top">
        <Container className="custom_container">
          <div className="row">
            <div className="col title_ctb_benefit">
              <h2>{title}</h2>
            </div>
          </div>
          <div className="row">
            <div className="col contain_line_ctb_benefit">
              <div className="line_ctb_benefit" />
            </div>
          </div>
          <div className="row contain_ctb_content">
            <div className="col detail_ctb_content">
              <p>
                {content}
              </p>
            </div>
          </div>
        </Container>
      </div>
      <Container className="custom_container ctn_package_tcb_kt">
        <div>
          <div className="package_ctb_overview">
            <div className="contain_package_ctb">
              <div className="ctn_medal_tcb">
                <div className="medal_detail medal1_bg">
                  <img src={"/img/landing/medal_1.png"} alt="" />
                </div>
              </div>
              <div className="tcb_title_package_hdi">
                <h3>HDI FlightDelay - gói 1</h3>
              </div>
              <div className="info_pgk_tcb_hdi">
                <div className="row m-0 mt-3">
                  <div className="col-1 p-0 icon_pgk_ctb">
                    <i class="fas fa-shield-alt"></i>
                  </div>
                  <div className="col-7 p-0 content_dt_pgk_tcb">
                    Trễ chuyến bay.
                  </div>
                  <div className="col-4 p-0 monney_pgk_tcb">10.000.000</div>
                </div>
                <div className="row m-0 mt-3">
                  <div className="col-1 p-0 icon_pgk_ctb">
                    <i class="fas fa-shield-alt"></i>
                  </div>
                  <div className="col-7 p-0 content_dt_pgk_tcb">
                    Hủy chuyến bay.
                  </div>
                  <div className="col-4 p-0 monney_pgk_tcb">50.000.000</div>
                </div>
                <div className="row m-0 mt-3">
                  <div className="col-1 p-0 icon_pgk_ctb">
                    <i class="fas fa-shield-alt"></i>
                  </div>
                  <div className="col-7 p-0 content_dt_pgk_tcb">
                    Chuyến bay quay đầu/đáp xuống sân bay khác
                  </div>
                  <div className="col-4 p-0 monney_pgk_tcb">50.000.000</div>
                </div>
                <div className="row m-0 mt-3">
                  <div className="col-1 p-0 icon_pgk_ctb">
                    <i class="fas fa-shield-alt"></i>
                  </div>
                  <div className="col-7 p-0 content_dt_pgk_tcb">
                    Hành lý đến trễ
                  </div>
                  <div className="col-4 p-0 monney_pgk_tcb">50.000.000</div>
                </div>
              </div>
              <div className="total_monney_tcb">
                <div className="row m-0">
                  <div className="col-4 label_tcb_hdi">Giá chỉ:</div>
                  <div className="col-8 total_tcb_hdi">10.000 VNĐ</div>
                </div>
              </div>
              <div className="button_buy_tcb">
                <button className="button_buy_tcb_detail btn1_buy_tcb">
                  Mua ngay
                </button>
              </div>
            </div>

            <div className="contain_package_ctb">
              <div className="ctn_medal_tcb">
                <div className="medal_detail medal2_bg">
                  <img src={"/img/landing/medal_2.png"} alt="" />
                </div>
              </div>
              <div className="tcb_title_package_hdi">
                <h3>HDI FlightDelay - gói 2</h3>
              </div>
              <div className="info_pgk_tcb_hdi">
                <div className="row m-0 mt-3">
                  <div className="col-1 p-0 icon_pgk_ctb">
                    <i class="fas fa-shield-alt"></i>
                  </div>
                  <div className="col-7 p-0 content_dt_pgk_tcb">
                    Trễ chuyến bay.
                  </div>
                  <div className="col-4 p-0 monney_pgk_tcb">10.000.000</div>
                </div>
                <div className="row m-0 mt-3">
                  <div className="col-1 p-0 icon_pgk_ctb">
                    <i class="fas fa-shield-alt"></i>
                  </div>
                  <div className="col-7 p-0 content_dt_pgk_tcb">
                    Hủy chuyến bay.
                  </div>
                  <div className="col-4 p-0 monney_pgk_tcb">50.000.000</div>
                </div>
                <div className="row m-0 mt-3">
                  <div className="col-1 p-0 icon_pgk_ctb">
                    <i class="fas fa-shield-alt"></i>
                  </div>
                  <div className="col-7 p-0 content_dt_pgk_tcb">
                    Chuyến bay quay đầu/đáp xuống sân bay khác
                  </div>
                  <div className="col-4 p-0 monney_pgk_tcb">50.000.000</div>
                </div>
                <div className="row m-0 mt-3">
                  <div className="col-1 p-0 icon_pgk_ctb">
                    <i class="fas fa-shield-alt"></i>
                  </div>
                  <div className="col-7 p-0 content_dt_pgk_tcb">
                    Hành lý đến trễ
                  </div>
                  <div className="col-4 p-0 monney_pgk_tcb">50.000.000</div>
                </div>
              </div>
              <div className="total_monney_tcb">
                <div className="row m-0">
                  <div className="col-4 label_tcb_hdi">Giá chỉ:</div>
                  <div className="col-8 total_tcb_hdi">50.000 VNĐ</div>
                </div>
              </div>
              <div className="button_buy_tcb">
                <button className="button_buy_tcb_detail btn2_buy_tcb">
                  Mua ngay
                </button>
              </div>
            </div>
            <div className="contain_package_ctb">
              <div className="ctn_medal_tcb">
                <div className="medal_detail medal3_bg">
                  <img src={"/img/landing/medal_3.png"} alt="" />
                </div>
              </div>
              <div className="tcb_title_package_hdi">
                <h3>HDI FlightDelay - gói 3</h3>
              </div>
              <div className="info_pgk_tcb_hdi">
                <div className="row m-0 mt-3">
                  <div className="col-1 p-0 icon_pgk_ctb">
                    <i class="fas fa-shield-alt"></i>
                  </div>
                  <div className="col-7 p-0 content_dt_pgk_tcb">
                    Trễ chuyến bay.
                  </div>
                  <div className="col-4 p-0 monney_pgk_tcb">10.000.000</div>
                </div>
                <div className="row m-0 mt-3">
                  <div className="col-1 p-0 icon_pgk_ctb">
                    <i class="fas fa-shield-alt"></i>
                  </div>
                  <div className="col-7 p-0 content_dt_pgk_tcb">
                    Hủy chuyến bay.
                  </div>
                  <div className="col-4 p-0 monney_pgk_tcb">50.000.000</div>
                </div>
                <div className="row m-0 mt-3">
                  <div className="col-1 p-0 icon_pgk_ctb">
                    <i class="fas fa-shield-alt"></i>
                  </div>
                  <div className="col-7 p-0 content_dt_pgk_tcb">
                    Chuyến bay quay đầu/đáp xuống sân bay khác
                  </div>
                  <div className="col-4 p-0 monney_pgk_tcb">50.000.000</div>
                </div>
                <div className="row m-0 mt-3">
                  <div className="col-1 p-0 icon_pgk_ctb">
                    <i class="fas fa-shield-alt"></i>
                  </div>
                  <div className="col-7 p-0 content_dt_pgk_tcb">
                    Hành lý đến trễ
                  </div>
                  <div className="col-4 p-0 monney_pgk_tcb">50.000.000</div>
                </div>
              </div>
              <div className="total_monney_tcb">
                <div className="row m-0">
                  <div className="col-4 label_tcb_hdi">Giá chỉ:</div>
                  <div className="col-8 total_tcb_hdi">100.000 VNĐ</div>
                </div>
              </div>
              <div className="button_buy_tcb">
                <button className="button_buy_tcb_detail btn2_buy_tcb">
                  Mua ngay
                </button>
              </div>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );

  const ComponentVNAT = (
      <div
          style={bgBenefit}
          className="ctn_benefit_ldhk_vnat"
      >
        <div className='ctn_benefit_ldhk_vnat_detail'>
          <Container className="custom_container">
            <div className="contain_vnat_infor">
              <div className={`row title_tnds_infor m-0 ${['tncn', 'ntn', 'vnat'].includes(type) ? "title_tnds_text_start" : ""}`}>
                <h2>{title}</h2>
              </div>
              <div className="row contain_vnat_content">
                <div className="col detail_vnat_content">
                  <p>{content}</p>
                </div>
              </div>
              <Slider {...settings}>
                {(infoPack || []).map((item, i) =>{
                  let description = JSON.parse(item.DESCRIPTION);
                  let dataBenefit = description[0]?.BENEFITS;
                  // console.log('description', description);
                  // console.log('dataBenefit', dataBenefit);
                  return(
                      <div className="contain_package_vnat">
                        <div className="row">
                          <div className="col-2 icon_pgk_vnat mt-2">
                            <img src={"/img/landing/img_benefit_pkg.png"} alt=""/>
                          </div>
                          <div className="col-10">
                            <div className="content_dt_pgk_vnat">
                              Việt Nam An Toàn
                              { ' ' + item.PACK_NAME}
                            </div>
                          </div>
                        </div>

                        <div className="container info_pgk_tcb_hdi">
                          {dataBenefit.map((vl,i) =>{
                            return(
                                <div className="row mt-3">
                                  <div className="col-1 p-0 icon_pgk_ctb">
                                    <i className="fas fa-shield-alt" />
                                  </div>
                                  <div className="col-7 p-0 content_dt_pgk_vnat">
                                    {vl.NAME}
                                  </div>
                                  <div className="col-4 p-0 monney_pgk_vnat">{lng.getLang() === "vi" ? vl.VAL_VI : vl.VAL_EN}</div>
                                </div>
                            )
                          })}
                        </div>
                        <div className="container">
                          <div className="row dv_eff_time_vnat">
                            <div className="col-7 p-0 txt_eff_time_vnat">
                              Thời gian hiệu lực
                            </div>
                            <div className="col-5 p-0 vl_eff_time_vnat">{description[0]?.TIME_INSURED}</div>
                          </div>
                        </div>

                        <div className="container total_monney_vnat">
                          <div className="row">
                            <div className="col-4 p-0 label_vnat_hdi">Giá chỉ:</div>
                            <div className="col-8 p-0 total_vnat_hdi">{description[0]?.FEES + ' ' + description[0]?.CURRENCY}</div>
                          </div>
                        </div>
                        {/*<a href={`${link_mobile}?pack_code=${description[0]?.PACK_CODE}`} className="button_buy_vnat">*/}
                        {/*  <button className="button_buy_vnat_detail btn1_buy_vnat">*/}
                        {/*   Mua ngay*/}
                        {/*  </button>*/}
                        {/*</a>*/}
                        {/*<a href={`${link_mobile}?pack_code=${description[0]?.PACK_CODE}`} className="button_buy_vnat">*/}
                        {isMobile ? (
                          <a href={`${link_mobile}?pack_code=${description[0]?.PACK_CODE}`} className="button_buy_vnat">
                            <button className="button_buy_vnat_detail btn1_buy_vnat">
                             Mua ngay
                            </button>
                          </a>
                        ) : (
                            <div className="button_buy_vnat">
                              <button className="button_buy_vnat_detail btn1_buy_vnat" onClick={handleRegister}>
                                Mua ngay
                              </button>
                            </div>
                        )}
                      </div>
                  )
                })
                }
              </Slider>
            </div>
          </Container>
        </div>
      </div>
  )

  const ComponentTAINAN = (
      <div style={{position: "relative"}}>
        <div className="hd-container packge-hdbank">
          <h1>{title}</h1>
          <p>{content}</p>
          <div className="hd-container">
            <div className="form-pkg-hdss view-positins">
              <Slider {...settings}>
                {(infoPack || []).map((data, index) =>{
                  let description = JSON.parse(data.DESCRIPTION);
                  let dataBenefit = description[0]?.BENEFITS;
                  return(
                      <div key={index} className="info-item-pkg-ss">
                        <div className="title-pkg-hdss">
                          <div className="icon_pgk_vnat">
                            <img src={"/img/landing/img_benefit_pkg.png"} alt=""/>
                          </div>
                          <div className="content_dt_pgk_hdss">
                            {data.PACK_NAME}
                          </div>
                        </div>
                        <div className="container info_pgk_tcb_hdi">
                          {dataBenefit.map((vl,i) =>{
                            return(
                                <div className="row mt-3">
                                      <div className="col-1 p-0 icon_pgk_ctb">
                                        {vl.NAME ? (
                                            <i className="fas fa-shield-alt" />
                                        ): (
                                            <i className="fas"/>
                                        )}
                                      </div>
                                  <div className="col-11 p-0 content_dt_pgk_vnat text-wrap">
                                    {vl.NAME}
                                  </div>
                                  {/*<div className="col-4 p-0 monney_pgk_vnat">{lng.getLang() === "vi" ? vl.VAL_VI : vl.VAL_EN}</div>*/}
                                </div>
                            )
                          })}
                        </div>
                        <div className="total_monney_vnat_hdss">
                          <div className="label_hdss_hdi">Giá chỉ:</div>
                          <div className="total_hdss_hdi">{`${description[0]?.FEES} ${description[0]?.CURRENCY}`}</div>
                        </div>
                        <div className="footer-item-package">
                              {!hideRegister?<div>
                                  {isMobile ? (
                                      <a href={`${link_mobile}?pack_code=${description[0]?.PACK_CODE}`}
                                         className="btn register-item-package">
                                        {l.g('bhsk.landing.register')}
                                      </a>
                                  ) : (
                                      <div onClick={handleRegister}
                                         className="btn register-item-package">
                                        {l.g('bhsk.landing.register')}
                                      </div>
                              )}
                           <p>
                            <a href={description[0]?.URL_DETAIL} target="_blank" className="detail-item-package">{'Chi tiết quyền lợi'}</a>
                          </p>
                        </div>:<a href={description[0]?.URL_DETAIL}
                                 className="btn register-item-package"
                                 style={{marginBottom:15}}
                                 target="_blank">
                                {'Chi tiết quyền lợi'}
                              </a>}
                  

                        
                         
                        </div>
                      </div>
                  )
                })}
              </Slider>
            </div>
          </div>
        </div>
        <div>
          <img className="img-background-package" src="/img/img_backround_package_insur.png"/>
        </div>
      </div>
  )


  switch (type) {
    case "tnds": {
      return ComponentDefaut;
    }
    case "tncn": {
      return ComponentDefaut;
    }
    case "tcb": {
      return ComponentTCB;
    }
    case 'ntn': {
      return ComponentDefaut;
    }
    case 'vnat': {
      return ComponentVNAT;
    }
    case 'tn365': {
      return ComponentTAINAN;
    }
    default: {
      return ComponentDefaut;
    }
  }
};

export default TndsInfomation;
