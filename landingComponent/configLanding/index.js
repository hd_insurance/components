export default {
  "bao-hiem-trach-nhiem-dan-su": {
    header: {
      bgHeader: true,
    },
    banner: {
      nameIsr: "TNDS Ô Tô",
      slogan: "An tâm xa lộ - Bảo hộ toàn dân",
      imgRight: "/img/landing/banner_tnds.png",
      background: "/img/landing/bannertop.png",
      type: "tnds"
    },
    introduceTop: {
      color: "white",
      colorNumber: "white",
      background: "/img/landing/bg_introducetop.png",
      data: [
        {
          title: "Giá chỉ từ",
          content: "480.000",
        },
        {
          title: "Bảo vệ người ngồi trên xe lên đến",
          content: "50 Triệu",
        },
        {
          title: "Số tiền bảo vệ đến",
          content: "150 Triệu",
        },
        {
          title: "Thời gian bảo vệ đến",
          content: "3 Năm",
        },
      ],
      type: "tnds",
    },
    introduce: {
      background: "/img/landing/intro_bg.png",
      imgLeft: "/img/landing/imgleft_introduce_tnds.png",
      content: "Bảo hiểm TNDS bắt buộc xe gắn máy là bảo hiểm mà người dân khi tham gia giao thông phải đem theo, và phái xuất trình được khi có cơ quan chức năng yêu cầu kiểm tra. Theo nghị định 03/2021/NĐ-CP của chính phủ về bảo hiểm TNDS bắt buộc với chủ xe cơ giới, người dân được sử dụng Bảo Hiểm TNDS điện tử.",
      data: [],
      type: "tnds",
    },
    benefit: {
      title: "Mức trách nhiệm dân sự ô tô",
      content: "",
      background: "",
      data: [
        {
          icon: "/img/landing/group_user.png",
          content: "Mức TNDS với người thứ Ba và hành khách",
        },
        {
          icon: "/img/landing/car.png",
          content: "Mức TNDS đối với Lái, phụ và người ngồi trên xe",
        },
        {
          icon: "/img/landing/box.png",
          content: "Mức TNDS của chủ xe với hàng hóa vận chuyển trên xe",
        },
        {
          icon: "/img/landing/security.png",
          content: "TNDS trong một số trường hợp khác",
        },
      ],
      type: "tnds",
    },
    form: {
      background: "/img/landing/bg_form.png",
      type: "tnds",
    },
    contact: {
      background: "/img/landing/bg_contact.png",
      image: "",
      type: "tnds",
    },
    relationProduct: {
      background: "/img/landing/bg_relation_product.png",
      type: "tnds",
    },
    appIntro: {
      background: "/img/landing/bg_intro_app.png",
      type: "tnds",
    },
  },
  // tai nan con nguoi
  "bao-hiem-tai-nan-con-nguoi": {
    header: {
      bgHeader: false,
    },
    banner: {
      nameIsr: "TAI NẠN CON NGƯỜI",
      slogan: "Sẻ chia gánh nặng Tài chính, An tâm tận hưởng cuộc sống.",
      imgRight: "/img/landing/banner_tncn.png",
      background: "/img/landing/background_banner_tncn.png",
      type: "tncn"
    },
    introduceTop: {
      color: "#323232",
      colorNumber: "#329945",
      background: "/img/landing/bg_introducetop_tncn.png",
      data: [
        {
          title: "Bệnh viện bảo lãnh khắp cả nước",
          content: "500",
        },
        {
          title: "Phòng khám 5 sao",
          content: "150",
        },
        {
          title: "Người mua bảo hiểm tai nạn mỗi năm",
          content: "300.000",
        },
        {
          title: "Trường hợp được chi trả bảo hiểm tai nạn",
          content: "200.000",
        },
      ],
      type: "tncn",
    },
    introduce: {
      background: "/img/landing/intro_bg.png",
      imgLeft: "/img/landing/imgleft_introduce_tncn.png",
      content: "Tài sản lớn nhất với mỗi con người luôn là sức khỏe. Nhưng trong cuộc sống hiện đại ngày nay, sức khỏe con người đang bị đe dọa bởi vô vàn nguy cơ từ tai nạn. Điều cần thiết ngay lúc này là bảo vệ bản thân và gia đình mình trước mọi rủi ro. Thấu hiểu được điều đó, Bảo hiểm HD- HDI mang đến cho bạn gói bảo hiểm tai nạn con người.",
      data: [{
        icon: "/img/landing/security.png",
        content: "Quyền lợi bảo hiểm lên tới 100.000USD/ người",
      },{
        icon: "/img/landing/support_money.png",
        content: "Hỗ trợ chi phí sau khi xuất viện",
      }
      ],
      type: "tncn",
    },
    benefit: {
      title: "Quyền lợi bảo hiểm chính",
      content: "",
      background: "/img/landing/bg_benefit_tncn.png",
      data: [
        {
          icon: "/img/landing/doctor.png",
          content: "Tử vong, thương tật toàn bộ vĩnh viễn do tai nạn",
        },
        {
          icon: "/img/landing/bag_doctor.png",
          content: "Mức TNDS đối với Lái, phụ và người ngồi trên xe",
        },
        {
          icon: "/img/landing/benefit.png",
          content: "Quyền lợi trợ cấp",
        },
        {
          icon: "/img/landing/security_white.png",
          content: "Tổng hợp Quyền lợi bảo hiểm Tai nạn con người",
        },
      ],
      type: "tncn",
    },
    form: {
      background: "/img/landing/bg_form.png",
      type: "tncn",
    },
    contact: {
      background: "/img/landing/bg_form_tncn.png",
      image: "/img/landing/img_contact_tncn.png",
      type: "tncn",
    },
    relationProduct: {
      background: "/img/landing/bg_relation_product.png",
      type: "tncn",
    },
    appIntro: {
      background: "/img/landing/bg_intro_app.png",
      type: "tncn",
    },
  },
  // tre chuyen bay
  "bao-hiem-tre-chuyen-bay": {
    header: {
      bgHeader: false,
    },
    banner: {
      nameIsr: "TRỄ CHUYẾN BAY",
      slogan: "Đồng hành cùng Khách hàng trọn an tâm mùa du lịch",
      imgRight: "/img/landing/banner_tcb.png",
      background: "/img/landing/background_banner_tcb.png",
      type: "tcb"
    },
    introduceTop: {
      color: "white",
      colorNumber: "white",
      background: "/img/landing/bg_introducetop_tcb.png",
      data: [
        {
          title: "Giá chỉ từ",
          content: "10.000",
        },
        {
          title: "Số tiền bồi thường khi xảy ra trễ chuyến lên đến",
          content: "5 Triệu",
        },
        {
          title: "Thời gian bồi thường chỉ trong vòng",
          content: "1 Tiếng",
        },
        {
          title: "Thời gian bảo vệ đến",
          content: "3 Năm",
        },
      ],
      type: "tcb",
    },
    introduce: {
      background: "/img/landing/intro_bg.png",
      imgLeft: "/img/landing/imgleft_introduce_tcb.png",
      content: "Bạn là người có sở thích đi du lịch? Bạn muốn yên tâm hơn trước những rủi ro có thể xảy ra trong chuyến đi? Bảo hiểm du lịch trong nước sẽ đem đến cho bạn có một chuyến đi thoải mái với rất nhiều tiện ích mà sản phẩm này mang lại.",
      data: [],
      type: "tcb",
    },
    benefit: {
      title: "Quyền lợi bảo hiểm chính",
      content: "Chi trả ngay toàn bộ số tiền bảo hiểm khi khách hàng gặp rủi ro trên đường du lịch ( VNĐ/ NGƯỜI/ CHUYẾN)",
      background: "",
      data: [],
      type: "tcb",
    },
    form: {
      background: "/img/landing/bg_form.png",
      type: "tcb",
    },
    contact: {
      background: "",
      image: "/img/landing/img_contact_tncn.png",
      type: "tcb",
    },
    relationProduct: {
      background: "/img/landing/bg_relation_product.png",
      type: "tcb",
    },
    appIntro: {
      background: "/img/landing/bg_intro_app.png",
      type: "tcb",
    },
  },
  // nha tu nhan
  "bao-hiem-nha-tu-nhan": {
    header: {
      bgHeader: false,
    },
    banner: {
      nameIsr: "NHÀ TƯ NHÂN",
      slogan: "Giúp khách hàng an tâm trước các rủi ro xảy ra với ngôi nhà.",
      imgRight: "/img/landing/banner_ntn.png",
      background: "/img/landing/background_banner_tncn.png",
      type: "ntn"
    },
    introduceTop: {
      color: "#323232",
      colorNumber: "#329945",
      background: "/img/landing/bg_introducetop_tncn.png",
      data: [
        {
          title: "Mức phí bảo hiểm tối thiểu",
          content: "550.000 VNĐ",
        },
        {
          title: "Mức hỗ trợ chi phí thuê nhà tối đa",
          content: "5% STBH",
        },
        {
          title: "Thời gian sử dụng tối đa của ngôi nhà",
          content: "15 năm",
        },
      ],
      type: "ntn",
    },
    introduce: {
      background: "/img/landing/intro_bg.png",
      imgLeft: "/img/landing/imgleft_introduce_ntn.png",
      content: "Cam kết luôn đồng hành, mang đến cho bạn sự an tâm trước các rủi ro về ngôi nhà, mái ấm yêu thương của gia đình bạn. Điều kiện tham gia:",
      data: [
        {
          icon: "/img/landing/user_ntn.png",
          content: "Là cán bộ nhân viên HDBank",
        },
        {
          icon: "/img/landing/house_ntn.png",
          content: "Đối tượng bảo hiểm là Nhà phố/ biệt thự/ chung cư có thời gian hoàn thiện xây dựng và đưa vào sử dụng tối đa 15 năm",
        }
      ],
      type: "ntn",
    },
    benefit: {
      title: "Quyền lợi bảo hiểm",
      content: "",
      background: "/img/landing/bg_benefit_tncn.png",
      data: [
        {
          icon: "/img/landing/lighting.png",
          content: "Bảo hiểm sét đánh trực tiếp ngôi nhà",
        },
        {
          icon: "/img/landing/fire.png",
          content: "Bảo hiểm cháy do nổ hoặc bất kỳ nguyên nhân nào khác",
        },
        {
          icon: "/img/landing/benefit.png",
          content: "Hỗ trợ thay đổi chỗ ở tạm thời như thuê nhà nơi khác sau tổn thất",
        },
      ],
      type: "ntn",
    },
    form: {
      background: "/img/landing/bg_form.png",
      type: "ntn",
    },
    contact: {
      background: "/img/landing/bg_form_tncn.png",
      image: "/img/landing/img_contact_tncn.png",
      type: "ntn",
    },
    relationProduct: {
      background: "/img/landing/bg_relation_product.png",
      type: "ntn",
    },
    appIntro: {
      background: "/img/landing/bg_intro_app.png",
      type: "ntn",
    },
  },
};
