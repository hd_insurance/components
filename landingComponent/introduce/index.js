import React, { useEffect, useState } from "react";
import { Container, Button } from "react-bootstrap";
import { isMobile } from "react-device-detect";

const Introduce = (props) => {
  const { background, imgLeft, content, data, type, title } = props.config;


  const [styleBg, setStyleBg] = useState({
    backgroundImage: `url(${background})`,
  });

  useEffect(() => {
    if (isMobile) {
      setStyleBg({ backgroundColor: "white" });
    }
  }, []);

  const ComponentTypeDefaut = (
    <div
      className={
        type === "tnds" ? "introduce_overview" : "introduce_overview_tncn"
      }
      style={styleBg}
    >
      <Container className="custom_container">
        <div className="row contain_introduce">
          <div className="col-md-6 contain_img_introduce">
            <img className="img_introduce" src={imgLeft} alt="introduce" />
          </div>
          <div className="col-md-6 content_introduce">
            <div className="title_introduce">
              <h2>{title?title:"Giới Thiệu"}</h2>
            </div>
            <div className="line_introduce" />
            <div className="content_introduce_detail">
              <p>{content}</p>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );

  const ComponentTypeTncn = (
    <div className="intro_type_tncn">
      <div className="contain_intro_tncn">
        <Container className="custom_container">
          <div className="row">
            <div className="col-md-6">
              <div className="kitin_tncn">
                <div className="particex_tncn" />
                <img src={imgLeft} alt="" className="style_imgleft_tncn" />
              </div>
            </div>
            <div className="col-md-6 custom_ctxx_tncn">
              <div className="title_introduce_tncn">
                <h2>{title?title:"Giới Thiệu"}</h2>
              </div>
              <div className="line_introduce_tncn" />
              <div className="content_introduce_tncn">
                <p>{content}</p>
              </div>
              {data.map((item, index) => {
                return (
                  <div className="row mt-4" key={index}>
                    {/* <div className="col-md-7">
                      <div className="row"> */}
                        <div className="col-2">
                          <div className="list_icon_tncn">

                            {item.icon_type=="fa"?<i class={`iconfa-introduce ${item.icon}`}></i>:<img src={item.icon} alt="" />}
                          </div>
                        </div>
                        <div className="col-9 content_icon_tncn">
                          <div>
                            {item.content}
                          </div>
                        </div>
                      {/* </div>
                    </div> */}
                  </div>
                );
              })}
            </div>
          </div>
        </Container>
      </div>
    </div>
  );

  const ComponentTypeVNAT = (
      <div className={"introduce_overview_tncn"} style={styleBg}>
        <Container className="custom_container">
          <div className="row contain_introduce">
            <div className="col-md-5 contain_img_introduce">
              <img className="img_introduce" src={imgLeft} alt="introduce" />
            </div>
            <div className="col-md-7 content_introduce ctn_intro_vnat">
              <div className="title_introduce_vnat">
                <h2>{title?title:"Giới Thiệu"}</h2>
              </div>
              {/*<div className="line_introduce" />*/}
              <div className="content_introduce_detail">
                <p dangerouslySetInnerHTML={{ __html: content }} />
              </div>
            </div>
          </div>
        </Container>
      </div>
  )

  const ComponentTypeTAINAN = (
      <div className={"introduce_overview_tncn"} style={styleBg}>
        <Container className="custom_container">
          <div className="row contain_introduce">
            <div className="col-md-5 contain_img_introduce">
              <img className="img_introduce" src={imgLeft} alt="introduce" />
            </div>
            <div className="col-md-7 content_introduce ctn_intro_vnat">
              <div className="title_introduce_vnat">
                <h2>{title}</h2>
              </div>
              {/*<div className="line_introduce" />*/}
              <div className="content_introduce_detail">
                <p dangerouslySetInnerHTML={{ __html: content }} />
                {data.map((item, index) => {
                  return (
                      <div className="row mt-4" key={index}>
                        {/* <div className="col-md-7">
                      <div className="row"> */}
                        <div className="col-1">
                            <i className={item.icon} style={{color: '#329945'}}  />
                        </div>
                        <div className="col-10 content_icon_tncn">
                          <div>
                            {item.content}
                          </div>
                        </div>
                        {/* </div>
                    </div> */}
                      </div>
                  );
                })}
              </div>
            </div>
          </div>
        </Container>
      </div>
  )

  switch (type) {
    case "tnds": {
      return ComponentTypeDefaut;
    }
    case "tncn": {
      return ComponentTypeTncn;
    }
    case "ntn": {
      // dung chung voi landing tncn
      return ComponentTypeTncn;
    }
    case 'vnat': {
      return ComponentTypeVNAT;
    }
    case 'tn365': {
      return ComponentTypeTAINAN;
    }
    default: {
      return ComponentTypeDefaut;
    }
  }
};

export default Introduce;
