import React, { useEffect, useState } from "react";
import { Container, Button } from "react-bootstrap";
import { useCookies } from "react-cookie";
const ProductList = (props) => {

  const {config} = props
  const [cookies, setCookie] = useCookies(["hdi_tracking"]);


  useEffect(() => {
    const timer=setTimeout(() => {
      setTimeLeft(calculateTimeLeft());
    }, 30000);

    // Clear timeout if the component is unmounted
    return () => clearTimeout(timer);
  });


  const calculateTimeLeft = () => {
    let year = new Date().getFullYear();
    let difference = +new Date(config.time_expired) - +new Date();
    let timeLeft = {};

    if (difference > 0) {
      timeLeft = {
        days: Math.floor(difference / (1000 * 60 * 60 * 24)),
        hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
        minutes: Math.floor((difference / 1000 / 60) % 60),
        seconds: Math.floor((difference / 1000) % 60)
    };
  }

  return timeLeft;

}

  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

  
  return (
    <div className="bg_product_promo" >
    
        <div className="tbasdsds">
          <Container className="custom_container">
          <div className="row">
            <div className="col-md-8">
              <p className="head-title" dangerouslySetInnerHTML={{__html: config.title}} ></p>
              <p className="sub-title">{config.subtitle}</p>
            </div>
            <div className="col-md-4">
              <div className="asdhsds">
               <a href="https://e-certificate.hdinsurance.com.vn/" target="_blank" style={{textDecoration: 'none'}}><div  className="btn-tracuu">TRA CỨU GIẤY CHỨNG NHẬN BẢO HIỂM</div></a>
              </div>
            </div>
          </div>

          
          </Container>
        </div>
        <Container className="custom_container">
          
          {/* <div className="time-promotion">
            <div className="time-title">Thời gian ưu đãi chỉ còn:</div>
            <div className="time-detail">
              <div className="time-item">{timeLeft.days} ngày</div>
              <div className="time-item aborder">{timeLeft.hours} giờ</div>
              <div className="time-item">{timeLeft.minutes} phút</div>
            </div>
          </div> */}



          <div className="promotion-product-list-item">
              {config.product.map((item, index)=>{
                return ( <div className="promo-item" key={index}>
                <div className="row">
                    <div className="col-md-2">
                      <div className="thumb">
                        <img src={`${item.thumb}`} />
                      </div>
                    </div>

                    <div className="col-md-4">
                      <div className="title-promo">{item.title}</div>
                    </div>

                    <div className="col-md-4">
                      <div className="title-description">{item.description}</div>
                    </div>

                    <div className="col-md-2">
                        <a href={cookies.hdi_tracking?`${item.link}?tracking=${cookies.hdi_tracking}`:item.link} target="_blank"><div className="view-detail">Chi tiết </div></a>
                    </div>
                  </div>
              </div>)
              })}
             

          </div>

        </Container>
      
    </div>
  );
};

export default ProductList;
