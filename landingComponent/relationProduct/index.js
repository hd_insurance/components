import React, { useEffect, useState, useContext } from "react";
import { Container, Button } from "react-bootstrap";
import Slider from "react-slick";

import PageContext from '../../../context/page_ctx'
import api from "../../../services/Network.js";
import globalStore from "../../../services/globalStore";


const Introduce = (props) => {
  const { background, type, product, slideShow, title, group} = props.config;
  const [relationProduct, setRelationProduct] = useState([]);

  const page_context = useContext(PageContext)
 

  const getInitRelationProduct = async (id) => {
    try {
      const group_id = group?group:"61c532627e11efaaab0d9729"
      var url_req = `/api/get-product/${group_id}`
      if(page_context.product){
        url_req = url_req+`?exc=${page_context.product}`
      }
      const relation_Product = await api.get(url_req);
      setRelationProduct(relation_Product.data);
    } catch (e) {
      console.log(e)
    }
  };

  useEffect(() =>{
    getInitRelationProduct(props.idQna);
  },[]);



  // console.log("product ", product.length)
  const SampleNextArrow = (props) => {
    const { className, style, onClick } = props;
    return (
      <div
        className={`${className} custom_arrow_left`}
        style={{ ...style }}
        onClick={onClick}
      >
        <img src="/img/landing/left-arrow.png" alt="left_arrow" />
      </div>
    );
  };

  const SamplePrevArrow = (props) => {
    const { className, style, onClick } = props;
    return (
      <div
        className={`${className} custom_arrow_right`}
        style={{ ...style }}
        onClick={onClick}
      >
        <img src="/img/landing/right-arrow.png" alt="right_arrow" />
      </div>
    );
  };
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: slideShow,
    slidesToScroll: slideShow,
    // className: "css_slick_product"
    responsive: [
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };

  return (
    <div
      className="ctn_bg_product_relation"
      style={{ backgroundImage: `url(${background})` }}
    >
      <Container className="custom_container kt_custom_relation_product">
        <div className="contain_product_infor">
          <div className="row title_product_infor m-0">
            <h2>{title?title:"Các sản phẩm thường được mua kèm"}</h2>
          </div>
          <div className="row title_product_infor m-0">
            <div className="line_product_infor" />
          </div>
          <div className="contain_relation_production">
            <div className="relation_production">
              <p className="m-0">
                Với mục đích mang đến trải nghiệm, quyền lợi và mức giá ưu đãi,
                <br className="hide_br_kt" />
                Bảo hiểm HD đã thiết kế ra các gói bảo hiểm khác nhau phù hợp
                với nhiều khách hàng.
              </p>
            </div>
          </div>
        </div>
        <Slider {...settings}>
          {(relationProduct || []).map((item, index) => {
            return (
              <div className="contain_slide_product">
                <div className="slide_product_detail">
                  <div className="slide_product_img">
                    <img src={item.thumbnail} alt="" />
                  </div>
                  <div className="title_product_slide">
                    <h5>{item.product_name[0]}</h5>
                  </div>
                  <div className="view_product_slide">
                    {/* <a href={item.benefit ? item.benefit : '/'}>Xem quyền lợi</a> */}
                  </div>
                  <div className="btn_view_product">
                    <a href={item.link} className="btn_view_product_detail" target="_blank">
                        Xem chi tiết
                    </a>
                  </div>
                </div>
              </div>
            );
          })}
        </Slider>
      </Container>
    </div>
  );
};

export default Introduce;
