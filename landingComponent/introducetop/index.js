import React, { useEffect, useState } from "react";
import TopBanner from "../../topbanner";
import Tilt from "react-tilt";
import { Container, Button } from "react-bootstrap";
import ReactHtmlParser from "react-html-parser";

const IntroduceTop = (props) => {
  const { color, colorNumber, background, type, data } = props.config;


  const ComponentTypeOne = (
    <div className="overview_intro">
      <Container className="custom_container">
        <div className="tnds_introduce_top">
          <div
            className="row gt-list"
            style={{ backgroundImage: `url(${background})` }}
          >
            {data.map((item, index) => {
              return (
                <div className="col-md col-6 custom_hqk_tt">
                  <div className="tnds_text_kt">
                    <p className="m-0" style={{ color }}>
                      {item.title}
                    </p>
                  </div>
                  <div className="tnds_title_kt">
                    <p className="m-0" style={{ color: colorNumber }}>
                       {ReactHtmlParser(item.content)}
                    </p>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </Container>
    </div>
  );

  const ComponentTypeTwo = (
    <div
      className="type_2_introducetop"
      style={{ backgroundImage: `url(${background})` }}
    >
      <Container className="custom_container">
        <div className="type_2_content_introduce row">
          {data.map((item, index) => {
            return (
              <div className="col-md col-6 custom_type2_col">
                <div className="tnds_title_kt">
                  <p className="m-0" style={{ color: colorNumber }}>
                     {ReactHtmlParser(item.content)}
                  </p>
                </div>
                <div className="tnds_text_kt">
                  <p className="m-0" style={{ color }}>
                    {item.title}
                  </p>
                </div>
              </div>
            );
          })}
        </div>
      </Container>
    </div>
  );

  switch (type) {
    case "tnds": {
      return ComponentTypeOne;
    }
    case "tncn": {
      return ComponentTypeTwo;
    }
    case "ntn": {
      return ComponentTypeTwo;
    }
    default: {
      return ComponentTypeOne;
    }
  }
};

export default IntroduceTop;
