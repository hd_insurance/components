import {Container} from "react-bootstrap";

const Footer = () => (
  <div className="footer"
    style={{background: 'white'}}
  >
    <Container className="custom_container">
      <div className="row">
        <div className="col-md-6">
          <div className="row">
            <div className="col-md-4">
              <div className="logo_hdi">
                <img src="/img/landing/logo_hdi.png" alt="HDI" />
              </div>
            </div>
            <div className="col-md-8">
              <h3 className="footer-h3 mt-20-responsive">
                {l.g("footer.title1")}
              </h3>
              <p className="m-item">
                <i className="fas fa-headphones-alt"></i>
                <a href="#"> 1900 068 898</a>
              </p>
              <p className="m-item">
                <i className="fas fa-envelope"></i>
                <a href="#"> cskh@hdinsurance.com.vn</a>
              </p>
              <p className="m-item">
                <i className="fas fa-map-marker-alt"></i>{" "}
                {l.g("footer.address")}
              </p>
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="row">
            <div className="col-md-5">
              {/* <h3 className="footer-h3 custom_ft_mobile">
                {l.g("footer.title2")}
              </h3>
              <ul className="footer-menu">
                <li>
                  <a href="/">{l.g("footer.menu.mn1")}</a>
                </li>
                <li>
                  <a href="/">{l.g("footer.menu.mn2")}</a>
                </li>
                <li>
                  <a href="/">{l.g("footer.menu.mn3")}</a>
                </li>
              </ul> */}
            </div>
            <div className="col-md-7">
              <h3 className="footer-h3 mt-20-responsive">
                {l.g("footer.title3")}
              </h3>
              <div className="input-email-subcriber">
                <input
                  type="email"
                  placeholder={"Email của bản"}
                />
                <button className="button-submit">
                  {l.getLang() == "en" ? "Send" : "Gửi"}
                </button>
              </div>
              <h3 className="footer-h3 mt-20">{l.g("footer.title4")}</h3>
              <div className="social-list">
                <a href="https://www.facebook.com/CongTyBaoHiemHD/">
                  <i className="fab fa-facebook-f"></i>
                </a>
                <a href="https://twitter.com/BaoHiemHD">
                  <i className="fab fa-twitter"></i>
                </a>
                <a href="https://www.youtube.com/channel/UCWGaAaE8tUp42xmOsDPuYzg">
                  <i className="fab fa-youtube"></i>
                </a>
                <a href="https://www.instagram.com/baohiemhd/">
                  <i className="fab fa-instagram"></i>
                </a>
                <a href="https://www.linkedin.com/company/hd-insurance-company-limited/">
                  <i className="fab fa-linkedin-in"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="footer-copy-right">
        <p>{`Copyright © ${new Date().getFullYear()} HDInsurance. All rights reserved`}</p>
      </div>
    </Container>
  </div>
);

export default Footer;
