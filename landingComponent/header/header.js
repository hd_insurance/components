import Link from "next/link";
import React, { useEffect, useState } from "react";
import {
  Container,
  ButtonGroup,
  Dropdown,
  DropdownButton,
  Button,
} from "react-bootstrap";
import api from "../../../services/Network.js";
import { slide as Menu } from "react-burger-menu";

const Header = (props) => {
  const config = props.config || {};
  const [lang, setLang] = useState(l.getLang() || "vi");
  const [isOpen, setOpen] = useState(false);
  const [bgHeader, setBgHeader] = useState("");

  useEffect(() => {
    if (config.bgHeader) {
      setBgHeader("bg_header_kitin");
    }
  }, [config.bgHeader]);

  useEffect(() =>{
    const ckv = getCookie("current_language")
    if(ckv){
      setLang(ckv)
    }else{
      setLang("vi")
    }
    
  },[l.getLang()]);

  useEffect(() =>{
    const ckv = getCookie("current_language")
    if(ckv){
      setLang(ckv)
      l.setLang(ckv);
    }else{
      l.setLang("vi");
    }
  },[]);

  var isMenuOpen = function (state) {
    setOpen(state.isOpen);
    return state.isOpen;
  };

 var getCookie = (name) => {
    var cookieValue = null
    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i].trim();
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
}
  const onLangSelect = async (lng) => {
    setLang(lng);
    l.setLang(lng);
    const result = await api.get(`/landing-api/current-language/${lng}`)
    window.location.reload(false);
  };

  const handleRegister = () => {
    props.scrollForm();
  }
  return (
    <nav
      className={`navbar navbar-expand-lg navbar-kitin-hdi bg-light-hdi ${config?.fixHeader ?`navbar-fixed-header` :null} ${bgHeader}`}
    >
      <Container className="custom_container">
        <a className="navbar-brand" href="#">
          <img src="/img/logo-hdi.svg" alt="HDI" />
        </a>

        {/* header cho màn mobile */}
        {
          config.isRegister && (<div className="register_mobile_hdi">
          <div className="btn_register_hdi">
            <Button className="btn_register_hdi_detail">
              <a href={config.link_mobile}
                 // target="_blank"
              >
                {config.title_register ? config.title_register : 'Đăng kí'}
              </a>
            </Button>
          </div>
            {!config?.is_hotline ? (
                <div className="contain_phone_header_hdi">
                  <a href={'tel:1900068898'}>
                    <img src="/img/landing/header_phone.png" alt="ff" />
                  </a>
                </div>
            ) : null}
            {config?.is_search && (
                <div className="contain_phone_header_hdi">
                  <a href={config?.is_search} target="_blank" >
                    <img src="/img/landing/ic_search.png" alt="ff" />
                  </a>
                </div>
            )}
            {config?.is_claim && (
                <div className="contain_phone_header_hdi">
                  <a href={config?.is_claim} target="_blank">
                    <img src="/img/landing/ic_claim.png" alt="ff" />
                  </a>
                </div>
            )}

        </div>)
        }
        
        {/* header cho màn mobile */}

        <button
          className="navbar-toggler"
          onClick={() => setOpen(true)}
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon">
            <i className="fas fa-bars"></i>
          </span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <div className="title-header-placholder">
            {config.contact && (
              <div className="webinfo">
                <p>
                  <a href={`tel:${config.contact.phone}`}>
                    <i className="fas fa-phone-alt"></i>
                    {config.contact.phone}
                  </a>
                </p>
                <p>
                  <a href={`mailto:${config.contact.email}`}>
                    <i className="fas fa-envelope"></i>
                    {config.contact.email}
                  </a>
                </p>
              </div>
            )}
          </div>

          <div className={`header-r-menu my-2 my-lg-0 ${bgHeader}`}>
            <DropdownButton
              as={ButtonGroup}
              title={
                <span style={{ paddingRight: 5 }}>
                  <i className="fas fa-globe-asia"></i>
                  {lang == "vi"
                    ? l.g("header.langtitle.vi")
                    : l.g("header.langtitle.en")}
                </span>
              }
              style={{paddingRight: '20px'}}
              className="btn-dropdown-lang custom-btn-lang"
            >
              <Dropdown.Item
                as="button"
                onSelect={() => onLangSelect("vi")}
                active={lang == "vi"}
                className="drop-item-mn"
              >
                <img src="/img/vi-flag.svg" />
                <div className="text">{l.g("header.langtitle.vi") }</div>{" "}
                {lang == "vi" ? <i className="fas fa-check-circle"/> : ""}
              </Dropdown.Item>
              {/*<Dropdown.Divider />*/}
              {config.mutil_language && (
                  <Dropdown.Item
                      as="button"
                      onSelect={() => onLangSelect("en")}
                      active={lang == "en"}
                      className="drop-item-mn"
                  >
                    <img src="/img/en-flag.svg" />
                    <div className="text">{l.g("header.langtitle.en")}</div>{" "}
                    {lang == "en" ? <i className="fas fa-check-circle"/> : ""}
                  </Dropdown.Item>
              )}

            </DropdownButton>

            {/* <DropdownButton
              as={ButtonGroup}
              title={
                <span style={{ paddingRight: 5 }}>
                  <i className="far fa-question-circle"></i>
                  {l.g("header.help.title")}
                </span>
              }
              className="btn-dropdown-lang"
            >
              <Dropdown.Item href="#/action-1" className="drop-item-mn">
                <img src="/img/hdsd-ic.svg" />
                <div className="text">{l.g("header.help.menu.item1")}</div>{" "}
                <i className="fas fa-chevron-right"></i>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item href="#/action-2" className="drop-item-mn">
                <img src="/img/dieukhoan-ic.svg" />
                <div className="text">{l.g("header.help.menu.item2")}</div>
                <i className="fas fa-chevron-right"></i>
              </Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Item href="#/action-3" className="drop-item-mn">
                <img src="/img/lienhe-ic.svg" />
                <div className="text">{l.g("header.help.menu.item3")}</div>
                <i className="fas fa-chevron-right"></i>
              </Dropdown.Item>
            </DropdownButton> */}
          </div>
          {/* header desktop phone  */}
     
              
              <div className="contain_register_desktop">
              {
                config.isRegister && (<div className="register_desktop_hdi">
                <Button className="btn_register_desktop_hdi" onClick={handleRegister}>{config.title_register ? config.title_register : 'Đăng kí'}</Button>
              </div>)
              }
                {!config?.is_hotline ? (
                    <div className="phone_desktop_hqk">
                      <a href={'tel:1900068898'}>
                        <img src="/img/landing/header_phone.png" alt="ff" />
                      </a>
                      <div className="content_phone_desktop">
                        <p className="m-0">Hotline:</p>
                        <p className="m-0">1900 068898</p>
                      </div>
                    </div>
                ) : null}
                {config?.is_search && (
                    <>
                      <div className="dv_search_gcn">
                        <a href={config?.is_search} target="_blank" >
                          <img src="/img/landing/ic_search.png" alt="ff" />
                        </a>
                      </div>
                      <div className="dv_line_icon" />
                    </>
                )}
                {config?.is_claim && (
                    <>
                      <div className="dv_search_gcn">
                        <a href={config?.is_claim} target="_blank" >
                          <img src="/img/landing/ic_claim.png" alt="ff" />
                        </a>
                      </div>
                    </>
                )}
            </div>
          {/* header desktop phone  */}
        </div>

        <Menu
          isOpen={isOpen}
          width={280}
          right
          customBurgerIcon={false}
          customCrossIcon={false}
          onStateChange={isMenuOpen}
        >
          <div className="lg-mobile">
            <DropdownButton
              as={ButtonGroup}
              title={
                <span style={{ paddingRight: 5 }}>
                  <i className="fas fa-globe-asia"></i>
                  {l.g("header.langtitle.vi")}
                </span>
              }
              className="btn-dropdown-lang"
            >
              <Dropdown.Item
                as="button"
                onSelect={() => onLangSelect("vi")}
                active={lang == "vi"}
                className="drop-item-mn"
              >
                <img src="/img/vi-flag.svg" />
                <div className="text">{l.g("header.langtitle.vi")}</div>
                {lang == "vi" ? <i className="fas fa-check-circle"></i> : ""}
              </Dropdown.Item>
               {/*<Dropdown.Divider />*/}
              {config.mutil_language && (
                  <Dropdown.Item
                      as="button"
                      onSelect={() => onLangSelect("en")}
                      active={lang == "en"}
                      className="drop-item-mn"
                  >
                    <img src="/img/en-flag.svg" />
                    <div className="text">{l.g("header.langtitle.en")}</div>{" "}
                    {lang == "en" ? <i className="fas fa-check-circle"></i> : ""}
                  </Dropdown.Item>
              )}
            </DropdownButton>
            <div className="btn-close-menu" onClick={() => setOpen(false)}>
              <i className="fas fa-times"></i>
            </div>
          </div>
          {/* <a id="home" className="menu-item" href="/">
            <img src="/img/hdsd-ic.svg" />
            {l.g("header.help.menu.item1")}
          </a>
          <a id="about" className="menu-item" href="/">
            <img src="/img/dieukhoan-ic.svg" />
            {l.g("header.help.menu.item2")}
          </a>
          <a id="contact" className="menu-item" href="/">
            <img src="/img/lienhe-ic.svg" />
            {l.g("header.help.menu.item3")}
          </a> */}
        </Menu>
      </Container>
    </nav>
  );
};
export default Header;
