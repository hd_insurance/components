import {Container} from "react-bootstrap";

const Step = ({config}) =>{
  
  return (
  <div>
    <Container className="custom_container step-progress-container">
      <div className="section-title-cs">
        <h1 className="section-title-1">{config.title}</h1>
        <div className="section-line"/>
      </div>
      <div className="row row-flex step-progress-block">
        {config.data.map((item, index)=>{
          return(<div className="step-progress-item col-md-4 col-6" key={index}>
                
                <div className="step-number">{index+1}</div>
                <div className="card-frame">
                    <div className="iconps">
                        <img src={item.icon}/>
                    </div>
                    <div className="content-x">
                      <p className="title">{item.title}</p>
                      <p className="sub-title">{item.subtitle}</p>
                    </div>
                  

                </div>

          </div>)
        })}
      </div>
    </Container>
  </div>
);
} 

export default Step;
