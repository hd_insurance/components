import React, { useEffect, useState } from "react";
import { Container, Button } from "react-bootstrap";

const IntroApp = (props) => {
  const {background, type} = props.config;
  
  return (
    <div className="bg_intro_app"
    style={{ backgroundImage: `url(${background})` }}
    >
      <Container className="custom_container">
        <div className="row contain_intro_app">
          <div className="col-md-7">
            <div className="row contain_content_intro_app">
              <h4>Tận hưởng cuộc sống</h4>
            </div>
            <div className="row slogan_content_intro_app">
              <div className="slogan_content_intro_one">
                <h4>Vững tâm tiến bước với</h4>
              </div>
              <div className="slogan_content_intro_two">
                <h4>Happy Digital Life</h4>
              </div>
            </div>
            <div className="row content_intro_app_detail">
              <p>
                Ứng dụng hàng đầu về bảo hiểm phi nhân thọ tại Việt Nam. Tải App
                ngay hôm nay để nhận những ưu đãi đặc biệt từ Happy Digital Life
              </p>
            </div>
            <div className="contain_code_qr">
              <div className="contain_code_qr_img p-0">
								<img src="/img/landing/qr_code.png" alt="" />
              </div>
							<div className="custom_or_qr">
                <p>hoặc</p>
              </div>
							<div className="store_app_kt">
                <div className="store_app_kt_google_play">
									<img src="/img/landing/google_play.png" alt="" />	
								</div>
								<div className="mt-3 store_app_kt_detail">
									<img src="/img/landing/app_store.png" alt="" />	
								</div>
              </div>
            </div>
          </div>
          <div className="col-md-5 right_phone">
            <img src="/img/landing/iphone_bg.png" alt="" />
          </div>
        </div>
      </Container>
    </div>
  );
};

export default IntroApp;
