import React, { useEffect, useState, createRef} from "react";
import FilePreviewer, {FilePreviewerThumbnail} from './preview';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";



const settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 5,
      slidesToScroll: 5
    };



const UploadPreview = (props) => {
  const [files, setFiles] = useState([])
  const [show, setShow] = useState(false)
  const [currentFile, setCurrentFile] = useState(props.files[0])
  const [currentIndex, setCurrentIndex] = useState(0)
  useEffect(() => {
    setShow(props.show)
  }, props.show);

const onItemThumbClick = (index, item)=>{
  setCurrentFile(item)
  setCurrentIndex(index)
}

const onFileDelete = (index)=>{
  props.removeFile(index)
}

return (
    <div className="dialog-preview-file-container">
      <div className="backdrop" onClick={(e)=>props.onClose()}></div>
      <div className="wraper-view-file">

          <div className="preview-item-file">
              <div className="prevw">
                <div className="controller">
                  <div className="delete-file" onClick={(e)=>onFileDelete(currentIndex)}>
                    <i class="far fa-trash-alt"></i>
                    Xoá
                  </div>
                </div>
                <div className="afr">
                  <FilePreviewer 
                    hideControls={true}
                    file={{url: currentFile.url}}
                  />
                </div>
              </div>

              
            </div>

          <div className="thumb-list">
            <Slider {...settings}>
              {props.files.map((item, index)=>{
                return(<div className="thumbnail-preview" onClick={(e)=>{onItemThumbClick(index, item)} }>
                          <FilePreviewerThumbnail file={{url: item.url}}/>
                      </div>)

              })}
            </Slider>
          </div>



        </div>
    </div>
);
}

export default UploadPreview;
