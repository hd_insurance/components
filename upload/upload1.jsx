import React, { useEffect, useState, createRef} from "react";
import {FilePreviewerThumbnail} from './preview';
import UploadFrame1 from "./uploadframe1"
import UploadFrame2 from "./uploadframe2"
import UploadPreview from "./dialogpreview"
import api from "../../services/Network.js";


const Upload1 = (props) => {
  const [config, setConfig] = useState({})
  const [files, setFiles] = useState([])
  const [listPreview, setListPreview] = useState(false)



  useEffect(() => {
    setConfig(props.config)
  }, [props.config]);


  const onFileUpload = async (out_file)=>{

      let formData = new FormData();

      formData.append("files", out_file[0]);
      const response =  await api.upload(formData)
      props.onFileUpload(config, response.data[0].file_key)

      const newf = files
      const fileReader = new FileReader();
      fileReader.onload = fileLoad => {
          const { result } = fileReader;
          newf.push({
            file: out_file[0],
            fileId: response.data[0].file_key,
            url: result
          })
          setFiles([...newf])



      };
      
      fileReader.readAsDataURL(out_file[0]);

  }


const showPreviewfile = (index)=>{
  setListPreview(true)
}

const removeFile = (index)=>{
  const fId = files[index].fileId
  props.onFileDelete(fId)
  const newarr = [ ...files ]
  newarr.splice(index, 1);
  setFiles(newarr)
  setListPreview(false)
}


return (
    <div className="upload-container div-flexbox">
    {listPreview&&<UploadPreview files={files} index={0} removeFile={removeFile} onClose={()=>setListPreview(false)}/>}

      {files.length>0?<div className="wrap-item1">
        <UploadFrame1 onFileUpload={onFileUpload}/>
      </div>:<div className="wrap-item2">
        <UploadFrame2 onFileUpload={onFileUpload}/>
      </div>}

      {((files.length>3)?files.slice((files.length - 3), files.length):files).map((item, index)=>{

        if(index==2){
            return( <div className="wrap-item1 last-item ml-10" key={index} onClick={(e)=>showPreviewfile(index)}>
                      {(files.length > 3)&&<div className="overlay-item">
                          <span className="span-text">+{files.length-3}</span>
                      </div>}
                      <div className="preview-item">
                        <FilePreviewerThumbnail file={{url: item.url}}/>
                      </div>
                    </div>)
        }else{
          return( <div className="wrap-item1 ml-10" key={index} onClick={(e)=>showPreviewfile(index)}>
            <div className="preview-item">
              <FilePreviewerThumbnail file={{url: item.url}} />
            </div>
          </div>)
        }

        
      })}
     
    </div>
);
}

export default Upload1;
