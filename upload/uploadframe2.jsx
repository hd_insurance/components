import React, { useEffect, useCallback, useState, createRef} from "react";
import Dropzone, {useDropzone} from 'react-dropzone'



const UploadFrame2 = (props) => {

   const [files, setFiles] = useState([])
   return(<div className="upload-item">
	    <Dropzone
	    accept={'image/jpeg, image/png, application/pdf'}
	    onDrop={f => {
	    	props.onFileUpload(f)
	    }}>
		  {({getRootProps, getInputProps}) => (
		    <div className="container">
		      <div
		        {...getRootProps({
		          className: 'dropzone',
		          onDrop: event => event.stopPropagation()
		        })}
		      >
		        <input {...getInputProps()} />
		         <i class="fas fa-cloud-upload-alt"></i> {lng.get("uzcnhtjw")}
		      </div>
		    </div>
		  )}
		</Dropzone>



    </div>
);
}

export default UploadFrame2;
