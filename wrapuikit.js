import React, { useState, useEffect } from "react";
import Csr from "./csr";
import {
  FLInput as FLInput_hdi,
  FLDate as FLDate_hdi,
  FLSelect as FLSelect_hdi,
  FLTextArea as FLTextArea_hdi,
  FLTime as FLTime_hdi,
  FLSelectMultiple as FLSelectMultiple_hdi,
  FLAddress as FLAddress_hdi,
  FLAmount as FLAmount_hdi,
  FLBankList as FLBankList_hdi,
  FLAirport as FLAirport_hdi,
  FLMultiSelect as FLMultiSelect_hdi,
} from "hdi-uikit";

const FLInput = (props) => {
  return (
    <Csr>
      <FLInput_hdi {...props} />
    </Csr>
  );
};
const FLDate = (props) => {
  return (
    <Csr>
      <FLDate_hdi {...props} />
    </Csr>
  );
};
const FLSelect = (props) => {
  return (
    <Csr>
      <FLSelect_hdi {...props} />
    </Csr>
  );
};
const FLTextArea = (props) => {
  return (
    <Csr>
      <FLTextArea_hdi {...props} />
    </Csr>
  );
};
const FLTime = (props) => {
  return (
    <Csr>
      <FLTime_hdi {...props} />
    </Csr>
  );
};
const FLSelectMultiple = (props) => {
  return (
    <Csr>
      <FLSelectMultiple_hdi {...props} />
    </Csr>
  );
};
const FLAddress = (props) => {
  return (
    <Csr>
      <FLAddress_hdi {...props} />
    </Csr>
  );
};
const FLAmount = (props) => {
  return (
    <Csr>
      <FLAmount_hdi {...props} />
    </Csr>
  );
};
const FLBankList = (props) => {
  return (
    <Csr>
      <FLBankList_hdi {...props} />
    </Csr>
  );
};
const FLAirport = (props) => {
  return (
    <Csr>
      <FLAirport_hdi {...props} />
    </Csr>
  );
};
const FLMultiSelect = (props) => {
  return (
    <Csr>
      <FLMultiSelect_hdi {...props} />
    </Csr>
  );
};
export {
  FLInput,
  FLDate,
  FLSelect,
  FLTextArea,
  FLTime,
  FLSelectMultiple,
  FLAddress,
  FLAmount,
  FLBankList,
  FLAirport,
  FLMultiSelect,
};


