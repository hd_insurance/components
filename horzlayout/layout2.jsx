import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import ReactHtmlParser, {
  processNodes,
  convertNodeToElement,
  htmlparser2,
} from "react-html-parser";
import PopUp from "./popup.js";

const View = (props) => {
  const data = {
    title: lng.get("287yieqb"),
    item: [
      {
        content: lng.get("v2rpg2td"),
        img: "/img/visa/ti1.svg",
        type: "scroll",
      },
      {
        title: "Doctor Anywhere",
        content: lng.get("t9joldao"),
        img: "/img/visa/ti2.svg",
        type: "doctor",
        listProduct: [
          {
            img: "/img/visa/d1.png",
            title: "KHÁM ONLINE",
          },
          {
            img: "/img/visa/d2.png",
            title: "BÁC SỸ UY TÍN",
          },
          {
            img: "/img/visa/d3.png",
            title: "MUA THUỐC TRỰC TUYẾN",
          },
          {
            img: "/img/visa/d4.png",
            title: "HỒ SƠ BỆNH ÁN TRỰC TUYẾN",
          },
          {
            img: "/img/visa/d5.png",
            title: "CHĂM SÓC LIÊN TỤC",
          },
          {
            img: "/img/visa/d6.png",
            title: "Y TẾ 4.0",
          },
        ],
      },
      {
        title: "Zutto Ride",
        content: lng.get("o5igm7hr"),
        img: "/img/visa/ti3.svg",
        type: "zutto",
        listProduct: [
          {
            img: "/img/visa/z1.png",
            title: "HỖ TRỢ 24/24",
            content:
              "Dịch vụ cứu hộ được chúng tôi cung cấp 24/24 kể cả ngày lễ và chủ nhật",
          },
          {
            img: "/img/visa/z2.png",
            title: "XE TẢI CHUYÊN DỤNG",
            content:
              "Khi cứu hộ bằng xe tải, chúng tôi sử dụng xe tải chuyên dụng với bàn nâng và dây ràng đảm bảo xe không bị ngã đỗ hay trầy xước",
          },
          {
            img: "/img/visa/z3.png",
            title: "CỨU HỘ TẬN NƠI",
            content:
              "Khi xe bạn gặp sự cố nhỏ nhân viên của chúng tôi sẽ đến tận nới để hỗ trợ bạn",
          },
          {
            img: "/img/visa/z4.png",
            title: "SỬA KHÓA TẬN NƠI",
            content:
              "Khi bạn bị trộm bẻ khóa hoặc mất chìa khóa nhân viên của chúng tôi sẽ đến hỗ trợ bạn kích hoạt cho xe hoạt động lại.",
          },
          {
            img: "/img/visa/z5.png",
            title: "GIAO XĂNG TẬN NƠI",
            content:
              "Khi bạn hết xăng giữa đường, nhân viên của chúng tôi sẽ mang xăng đến tận nơi để bạn có thể tiếp tục di chuyển.",
          },
          {
            img: "/img/visa/z6.png",
            title: "HỖ TRỢ TOÀN QUỐC",
            content:
              "Chúng tôi có nhân viên khắp mọi nơi đảm bảo sẽ hỗ trợ bạn 24/24 tại mọi miền trên toàn quốc.",
          },
        ],
      },
      {
        content: lng.get("t4morzkk"),
        img: "/img/visa/ti4.svg",
        type: "open-link",
      },
    ],
  };
  const [curentDataModal, setCurentDataModal] = useState({});
  const [isShowModal, setShowModal] = useState(false);
  const setOpenModal = (item) => {
    if (item.type == "zutto" || item.type == "doctor") {
      setCurentDataModal(item);
      setShowModal(true);
    } else if (item.type == "open-link") {
      window.open(
        "https://www.hdbank.com.vn/vi/personal/promotion/the",
        "_blank"
      );
    } else if (item.type=="scroll"){
      props.scrollToQL(2)
    }
  };
  return (
    <div className="hd-container">
      <PopUp data={curentDataModal} show={isShowModal} setShow={setShowModal} />
      <div className="horz-layout-dn-2">
        <h1 className="uppercase-first-letter">{data.title}</h1>
        <div className="row gt-list">
          {data.item.map((item, index) => {
            return (
              <div className="col-md-3 col-6" key={index}>
                <div className="gt-item" onClick={() => setOpenModal(item)}>
                  <div className="circle-k">
                    <img src={item.img} />
                  </div>
                  <p>{ReactHtmlParser(item.content)}</p>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default View;
