import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import Particles from "react-particles-js";
const _ = require("lodash");


const params = {
  autoPlay: true,
  fullScreen: {
    enable: false,
    zIndex: -1,
  },
  fpsLimit: 60,
  particles: {
    bounce: {
      opacity: 1,
      horizontal: {
        random: {
          enable: false,
          minimumValue: 0.1,
        },
        value: 1,
      },
      vertical: {
        random: {
          enable: false,
          minimumValue: 0.1,
        },
        value: 1,
      },
    },

    color: {
      value: "#71A853",
      animation: {
        enable: false,
        speed: 100,
        sync: false,
      },
    },

    move: {
      angle: {
        offset: 45,
        value: 90,
      },
      attract: {
        enable: false,
        rotate: {
          x: 3000,
          y: 3000,
        },
      },
      direction: "none",
      distance: 0,
      enable: true,
      gravity: {
        acceleration: 9.81,
        enable: false,
        maxSpeed: 50,
      },

      random: false,
      size: false,
      speed: 2.2,
      straight: false,
      trail: {
        enable: false,
        length: 10,
        fillColor: {
          value: "#000000",
        },
      },
      vibrate: false,
      warp: false,
    },
    number: {
      limit: 0,
      value: 15,
    },
    opacity: {
      random: {
        enable: true,
        minimumValue: 0.1,
      },
      value: 0.8,
      animation: {
        destroy: "none",
        enable: true,
        minimumValue: 0.5,
        speed: 0.5,
        startValue: "random",
        sync: false,
      },
    },
    rotate: {
      random: {
        enable: false,
        minimumValue: 0,
      },
      value: 0,
      animation: {
        enable: false,
        speed: 0,
        sync: false,
      },
      direction: "clockwise",
      path: false,
    },
    shape: {
      options: {},
      type: "circle",
    },
    size: {
      random: {
        enable: true,
        minimumValue: 1,
      },
      value: 15,
      animation: {
        destroy: "none",
        enable: true,
        minimumValue: 1,
        speed: 2,
        startValue: "random",
        sync: false,
      },
    },
    shadow: {
      enable: true,
      color: "#3CA9D1",
      blur: 5,
    },
  },
  themes: [],
};

const View = (props) => {
  const data = {
    title: lng.get("yrxrpiur"),
    item: [
      lng.get("yujdkcfo"),
      lng.get("nkvsqkoa"),
      lng.get("mao0ps6u"),
      lng.get("2kz0tzrq"),
    ],
  };

  if (!_.isEmpty(props.cardOwner)) {
    return <div></div>;
  } else {
    return (
      <div className="horz-layout-dn">
        <Particles className={"cs_particles"} width={"100%"} height={"100%"} params={params} />

        <div className="hd-container">
          <div className="row">
            <div className="col-md-6">
              <div className="left-img">
                <img src="/img/visa/hdan.png" />
              </div>
            </div>
            <div className="col-md-6 content-ctn">
              <div className="content">
                <h1 className="uppercase-first-letter">{data.title}</h1>
                <div className="list-content">
                  {data.item.map((item, index) => {
                    return (
                      <div className="item" key={index}>
                        <i className="fas fa-shield-alt"></i>
                        <p>{item}</p>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
};


export default View;
