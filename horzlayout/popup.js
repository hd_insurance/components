import React, { useEffect } from "react";
import { Modal } from "react-bootstrap";

export default function PopUp(props) {
  const renderItem = (item) => {
    switch (props.data.type) {
      case "doctor":
        return (
          <div className="col-md-4 mt-15 col-6">
            <img className="item-img-popup" src={item.img} />
            <div className="title-item-popup">{item.title}</div>
          </div>
        );
      case "zutto":
        return (
          <div className="col-md-4 mt-15 text-center col-6">
            <img className="icon-zutto" src={item.img} />
            <div className="title-item-popup">{item.title}</div>
            <p className="mt-15">{item.content}</p>
          </div>
        );
      default:
        return <div></div>;
    }
  };
  const renderFooter = () => {
    switch (props.data.type) {
      case "doctor":
        return (
          <>
            <div className="row">
              <div className="col-md-4 qr-visa">
                <div className="scan-qr-visa">Quét mã QR để tải app:</div>
                <img className="qr-visa-popup" src="/img/visa/doctorqa.png" />
              </div>
              <div className="col-md-8">
                <p>Hoặc tải app tại:</p>
                <p>
                  <span>- Android: </span>
                  <a
                    target="_blank"
                    href="https://play.google.com/store/apps/details?id=com.doctoranywhere"
                  >
                    Doctor Anywhere - Ứng dụng trên Google Play
                  </a>
                </p>
                <p>
                  <span>- App Store: </span>
                  <a
                    target="_blank"
                    href="https://apps.apple.com/vn/app/doctor-anywhere/id1273714922?l=vi"
                  >
                    Doctor Anywhere trên App Store (apple.com)
                  </a>
                </p>
                <p>
                  <span>- Hướng dẫn sử dụng app: </span>
                  <a
                    target="_blank"
                    href="https://www.youtube.com/watch?v=sWeMz-wzmCw"
                  >
                    https://www.youtube.com/watch?v=sWeMz-wzmCw
                  </a>
                </p>
                <p>
                  <span>- Trang chủ: </span>
                  <a
                    target="_blank"
                    href="https://doctoranywhere.vn/pages/tu-van-suc-khoe-truc-tuyen"
                  >
                    https://doctoranywhere.vn/pages/tu-van-suc-khoe-truc-tuyen
                  </a>
                </p>
              </div>
            </div>
            <div className="row justify-content-center">
              <div className="col-md-3 mt-15">
                <button
                  onClick={() => props.setShow(false)}
                  type="button"
                  className="btn btn-primary button-close-popup-visa"
                >
                  Đóng
                </button>
              </div>
            </div>
          </>
        );
      case "zutto":
        return (
          <>
            <div className="row">
              <div className="col-md-12">
                <p>Hướng dẫn thông báo sự cố:</p>
                <p>
                  Gọi điện trực tiếp lên tổng đài: <a href="#">1900 969612</a>{" "}
                  báo biển kiểm soát và thông báo sự cố đang phát sinh và vị trí
                </p>
                <p>
                  Xem chi tiết tại:{" "}
                  <a target="_blank" href="http://zuttoride.vn/">
                    http://zuttoride.vn/
                  </a>
                </p>
              </div>
            </div>
            <div className="row justify-content-center">
              <div className="col-md-3 mt-15">
                <button
                  onClick={() => props.setShow(false)}
                  type="button"
                  className="btn btn-primary button-close-popup-visa"
                >
                  Đóng
                </button>
              </div>
            </div>
          </>
        );
      default:
        return <div></div>;
    }
  };
  return (
    <>
      <Modal
        show={props.show}
        onHide={() => props.setShow(false)}
        dialogClassName="modal-popup-visa"
      >
        <Modal.Header closeButton>
          <Modal.Title>{props.data.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            {props.data.listProduct
              ? props.data.listProduct.map((item, i) => {
                  return renderItem(item);
                })
              : null}
          </div>
        </Modal.Body>
        <Modal.Footer>{renderFooter()}</Modal.Footer>
      </Modal>
    </>
  );
}
