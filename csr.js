import React, { useState, useEffect } from "react";

const useIsSsr = () => {
  const [isSsr, setIsSsr] = useState(true);
  useEffect(() => {
    setIsSsr(false);
  }, []);

  return isSsr;
};
const Csr = ({ children }) => {
  const isSsr = useIsSsr();
  if (isSsr) return null;
  return <React.Fragment>{children}</React.Fragment>;
};

export default Csr;
