import React from "react";

const LoadingForm = (props) => {
  return (
    <div className={props.isFullScreen ? "cover-full-loading" : ""}>
      <div className={props.isFullScreen ? "fm-loader-full" : "fm-loader"}>
        <div className="loader-sdk">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    </div>
  );
};
export default LoadingForm;
