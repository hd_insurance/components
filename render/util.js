import { createRef } from "react";
import LayoutModel from "../models/layoutModel";

import randomstring from "randomstring";

const formatCurrency = (value) => {
  return currencyFormatter.format(value, {
    code: "VNĐ",
    precision: 0,
    format: "%v %s",
    symbol: "VNĐ",
  });
};

const converObjectComponent = (layout, maplst, state) => {
  var listLayout = [];
  layout.map((item, index) => {
    var newObj = new LayoutModel();

    newObj.template_id = item.template_id;
    newObj.name = item.name;
    newObj.type = item.type;
    newObj.extend_class = item.extend_class;
    newObj.define = item.define;
    newObj.mapstate = item.mapstate;
    newObj.ref = createRef();
    if (item.col) {
      newObj.col = item.col;
    }
    if (item.layout) {
      newObj.layout = converObjectComponent(item.layout, maplst, state);
    }
   
    if (item.define) {
       if (item.define.layout) {
          newObj.layout = converObjectComponent(item.define.layout, maplst, state);
        }
    }
    if(state){
      if(state["view_"+item.template_id]){
        newObj.properties = state
      }else{
        state["view_"+item.template_id] = null
        newObj.properties = state
      } 
    }else{
      newObj.properties = {}
    }
    maplst[item.template_id] = newObj;
    listLayout.push(newObj);
    return 1;
  });
  return listLayout;
};



const rootObjectComponent = (define, state) => {

  var map = {};
  const obj_config = {
    template_id: define.template_id,
    type: define.type,
    name: define.name,
    layout: converObjectComponent(define.layout, map, state),
  };

  return { obj_config, map };
};





const randomID = () => {
  return randomstring.generate(7);
};
const isEmptyObj = (obj) => {
  return obj && Object.keys(obj).length === 0 && obj.constructor === Object;
};
const insert = (arr, index, newItem) => [
  ...arr.slice(0, index),

  newItem,

  ...arr.slice(index),
];
const getPrositionbyId = (list, id) => {
  return list.findIndex((x) => x.id === id);
};


const getParseSchema = (data, id)=>{
  var data_return = data
  id.split("/").forEach((item, index)=>{
    if(index>0){
      if(data_return[item]){
        data_return = data_return[item]
      }else{
        return null
      }
      
    }
  })

  return data_return


}


const formSchemaInstance = async (jsonschema)=>{
  // try {
  //   let schema = await $RefParser.dereference(jsonschema);
  //   console.log("cho do 2",schema);
  // }
  // catch(err) {
  //   console.error(err);
  // }

  // if(jsonschema.properties){
  //   for (const [key, value] of Object.entries(jsonschema.properties)) {
  //     console.log("item ", key, value.type)
  //     if(value.type == "object"){
  //         formSchemaInstance(value)
  //     }
  //   }
  // }else{

  // }
}



export default {
  // convertData,
  rootObjectComponent,
  isEmptyObj,
  insert,
  randomID,
  getPrositionbyId,
  formatCurrency,
  formSchemaInstance,
  getParseSchema
};
