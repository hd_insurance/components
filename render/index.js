import React, { useEffect, useState, createRef, forwardRef } from "react";
import {
  FLInput,
  FLDate,
  FLAddress,
  FLSelect,
  FLRadio,
  FLCheckBox,
  FLSwitch,
  FLAmount,
  FLTime,
  FLYear,
  Tabs,
  TabsItem,
  DivRender,
  UploadFrame,
  Text,
  OTPConfirm,
  BeneficiaryBank,
  BeneficiaryUser,
  SingleUpload
} from "hdi-uikit";

import Header from "../landingComponent/header/header.js";
import TopBanner from "../landingComponent/topbanner/topbanner.js";
import StepProgress from "../landingComponent/progress_step";
import IntroduceTop from "../landingComponent/introducetop/index.js"; 
import Introduce from "../landingComponent/introduce/index.js";
import TndsInfo from "../landingComponent/tnds/index.js";
import FormBH from "../landingComponent/form/index.js";
import QNA from "../landingComponent/qna/index.js";
import Contact from "../landingComponent/contact/index.js";
import ProductRelation from "../landingComponent/relationProduct/index.js";
import AppIntro from "../landingComponent/appintro/index.js";
import Footer from "../landingComponent/footer/index.js";
import Social from "../landingComponent/social/index.js";
import ProductPromotion from "../landingComponent/list_product_promotion/index.js";


import HeaderHealth from "../ComponentNew/header";
import BannerHealth from "../ComponentNew/top-banner";
import AdvantageProgram from "../ComponentNew/advantages-program";
import Conditions from "../ComponentNew/conditions";
import PackageInsurance from "../ComponentNew/package-insurance";
import Tutorial from "../ComponentNew/tutorial";
import SliderCaptcha from "../slider-captcha";
import SupportCustomer from "../ComponentNew/support-customer";
import FaqOld from "../ComponentNew/faq-old";
import ProductList from "../ComponentNew/list-product";
import DocsDownload from "../ComponentNew/document-dowload";
import IntroBannerApp from "../ComponentNew/banner-introduce-app";
import FooterOld from "../ComponentNew/footer"; 
import Component from "../component"; 
import FormRender from "../form"; 


import { Button, Tab, Nav } from "react-bootstrap";
import ReactHtmlParser from "react-html-parser";


const Layout = forwardRef((props, ref) => {
  // console.log('=====> props', props);
  const level = props.level ? parseInt(props.level) : 0;
  const hasLayout = (item) => {
    return item.layout && item.layout.length > 0;
  };
  const { layout, from, productConfig } = props;

  const renderLayout = (callback) => {

    if (level == 0) {
      switch (layout.type) {
        case "screen":
          return <Layout {...props} layout={layout.layout} level={level + 1} />;
        default:
          return <div></div>;
      }
    } else {
      return layout.map((item, i) => {
        if (level == 1) {
          if (layout.length - 1 == i) {
            setTimeout(() => {
              if (callback) {
                callback();
              }
            }, 200);
          }
        }
        //for loop to render layout
        switch (item.type) {
          case "button":
            let component_props_pop = {};
            if (item.mapstate) {
              component_props_pop.onClick = props[item.mapstate.state_onClick];
            }
            if(item.define.disabled){
              component_props_pop.disabled = item.define.disabled
            }
            if(item.properties){
              component_props_pop = {...component_props_pop, ...item.properties["view_"+item.template_id]}
            }
            if(component_props_pop.hide){
              return null
            }
            return (
              <Button
                key={`${from}-level-${level}-${i}`}
                className={`${item.extend_class} ${item.define?.isHide ? "class-hidden" : ""}`}
                onClick={() =>{
                  if(item.onClick){
                     item.onClick()
                  }
                }}
                {...component_props_pop}
                dangerouslySetInnerHTML={{ __html: item.define.value}}
              >
              </Button>
            );
          case "img":
            return (
              <img
                key={`${from}-level-${level}-${i}`}
                className={item.extend_class}
                src={item.define.url}
                style={{ width: item.define.width, height: item.define.height }}
              />
            );
          case "div":
            var item_row_props = {hide: false}
            if(item.properties){
              item_row_props = item.properties["view_"+item.template_id]||{}
            }
            let div_props_pop = {};
            if (item.mapstate) {
              div_props_pop.onClick = props[item.mapstate.state_onClick];
            }
            return (

              (item_row_props.hide)?null:<div 
                className={`${item.define.extend_class} ${
                  item.define?.isHide ? "class-hidden" : ""
                }`}
                key={`${from}-level-${level}-${i}`}
                {...div_props_pop}
                >
               
                {hasLayout(item) && !item?.define?.isHide && (
                  <Layout {...props} layout={item.layout} level={level + 1} />
                )}
              </div>
            );
          case "page_header":
            return <Header config={item.define} scrollForm={item.onScrollForm} key={`${from}-level-${level}-${i}`}/>
          case "page_top_banner":
            return <TopBanner config={item.define} key={`${from}-level-${level}-${i}`}/>
          case "top_introduce":
            return <IntroduceTop config={item.define} key={`${from}-level-${level}-${i}`}/>
          case "horizontal_intro":
            return <Introduce config={item.define} key={`${from}-level-${level}-${i}`}/>
          case "slide_block_1":
            return <TndsInfo config={item.define} scrollForm={item.onScrollForm} configProduct={productConfig} key={`${from}-level-${level}-${i}`}/>
          case "page_contact_1":
            return <Contact config={item.define} key={`${from}-level-${level}-${i}`}/>
          case "product_list_1":
            return !item.define?.isHide && <ProductRelation config={item.define} key={`${from}-level-${level}-${i}`}/>
          case "app_download":
            return (
              !item.define?.isHide && (<AppIntro config={item.define} key={`${from}-level-${level}-${i}`}/>)
            );
          case "page_footer":
            return <Footer config={item.define} key={`${from}-level-${level}-${i}`}/>
          case "social_bubble":
            return (
              !item.define?.isHide && (<Social config={item.define} key={`${from}-level-${level}-${i}`}/>)
            );
            case "form":
            var item_form_props = {hide: false}
            if(item.properties){
              item_form_props = item.properties["view_"+item.template_id]||{}
            }
            if(item_form_props.hide){
              return null
            }
              return (
                <FormRender
                  key={`${from}-level-${level}-${i}`}
                  ref={item.ref}
                  extend_class={item.define.extend_class}
                >
                  {hasLayout(item) && (
                    <Layout {...props} layout={item.layout} level={level + 1} />
                  )}
                </FormRender>
            
            );
          case "sdk_form":
            // console.log("item.define ", item.define.config)
            return <FormBH config={item.define} ref={item.ref} configProduct={item.define.config} key={`${from}-level-${level}-${i}`}/>
          case "page_qna":
            return <QNA config={item.define}/>

          case "list_product_promotion":
            return <ProductPromotion config={item.define} key={`${from}-level-${level}-${i}`}/>

          case "header_2": {
            return <HeaderHealth config={item.define} key={`${from}-level-${level}-${i}`} />
          }

          case "banner_2": {
            return <BannerHealth config={item.define} key={`${from}-level-${level}-${i}`} />
          }
          
          case "advantages-program": {
            return <AdvantageProgram config={item.define} key={`${from}-level-${level}-${i}`} />
          }

          case "conditions": {
            return <Conditions config={item.define} key={`${from}-level-${level}-${i}`} />
          }

          case "package-insurance": {
            return <PackageInsurance config={item.define} key={`${from}-level-${level}-${i}`} />
          }

          case "tutorial": {
            return <Tutorial config={item.define} key={`${from}-level-${level}-${i}`} />
          }
          case "step_progress": {
            return <StepProgress config={item.define} key={`${from}-level-${level}-${i}`} />
          }

          case "support_customer": {
            return <SupportCustomer config={item.define} key={`${from}-level-${level}-${i}`} />
          }

          case "faq_2": {
            return <FaqOld config={item.define} key={`${from}-level-${level}-${i}`} />
          }
          case "para": {
            return <p key={`${from}-level-${level}-${i}`} className={`${item.extend_class}`}>{item.define.value}</p>
          }

          case "product_list_2": {
            return !item.define?.isHide && <ProductList config={item.define} key={`${from}-level-${level}-${i}`}/>
          }

          case "docs_download": {
            return <DocsDownload config={item.define} key={`${from}-level-${level}-${i}`} />
          }

          case "introduce_banner_app": {
            return <IntroBannerApp config={item.define} key={`${from}-level-${level}-${i}`} />
          }

          case "footer_2": {
            return <FooterOld config={item.define} key={`${from}-level-${level}-${i}`} />
          }
          case "text":
             if(item.properties){
              if(item.properties["view_"+item.template_id]){
                if(item.properties["view_"+item.template_id].value){
                  return item.properties["view_"+item.template_id].value
                }
              }
             }
            return (item.define.value);
          case "fa-icon":
            let fa_props_pop = {};
            if (item.mapstate) {
              fa_props_pop.onClick = props[item.mapstate.state_onClick];
            }
            return (
              <i class={`fas ${item.define.icon}`} key={`${from}-level-${level}-${i}`} {...fa_props_pop}/>
          );
          case "slide_captcha_btn": {

            let component_props_pop = {};
            if (item.mapstate) {
              component_props_pop.callback = props[item.mapstate.onCallback];
              component_props_pop.onClick = props[item.mapstate.onClick];
              component_props_pop.ctcOpen = props[item.mapstate.ctcOpen];
              component_props_pop.loading = props[item.mapstate.loading];
            } else {
              
            }


            return  <SliderCaptcha
                      create="/captcha/create"
                      verify="/captcha/verify"
                      text={{challenge: item.define.challenge, anchor: item.define.anchor, btn_text: item.define.btn_text}}
                      {...component_props_pop}
                    />
          }
          //form
          case "single_select":
            let component_props_sglc = {};
            if (item.mapstate) {
              component_props_sglc.value = props[item.mapstate.state_value];
              if(props[item.mapstate.state_data]){
                 component_props_sglc.data = props[item.mapstate.state_data]
              }

              if(props[item.mapstate.state_changeEvent]){
                component_props_sglc.changeEvent = props[
                  item.mapstate.state_changeEvent
                ].bind(this, item.template_id);
              }
              
            } else {
              component_props_sglc.value = item.define.value;
              component_props_sglc.changeEvent = item.onValueChange;
            }
            return (
              <FLSelect
                key={`${from}-level-${level}-${i}`}
                data={item.define.list}
                label={item.define.label}
                required={item.define.is_required}
                disable={item.define.disable}
                value={item.define.value}
                template_id={item.template_id}
                changeEvent={item.onValueChange}
                extend_class={item.extend_class}
                hideborder={item.define.hideborder}
                ref={item.ref}
                component_obj={item}
                {...component_props_sglc}
              />
            );
           case "flinput":
            let component_props = {};
            if (item.mapstate) {
              component_props.value = props[item.mapstate.state_value];
              if(props[item.mapstate.state_changeEvent]){
                component_props.changeEvent = props[
                  item.mapstate.state_changeEvent
                ].bind(this, item.template_id);
              }
              
            } else {
              component_props.value = item.define.value;
              component_props.changeEvent = item.onValueChange;
            }

            return (
              <FLInput
                key={`${from}-level-${level}-${i}`}
                value={item.define.value}
                isUpperCase={item.define.isUpperCase}
                label={item.define.label}
                required={item.define.is_required}
                disable={item.define.disable}
                template_id={item.template_id}
                changeEvent={item.onValueChange}
                hideborder={item.define.hideborder}
                extend_class={item.extend_class}
                ref={item.ref}
                component_obj={item}
                pattern={item.define.pattern}
                {...component_props}
              />
            );
          case "bst_tabs":

            if(item.define.isHide){
              return null
            }
            let tab_component_props = {};
            if (item.mapstate) {

              if(props[item.mapstate.current_tab]){
                tab_component_props.defaultActiveKey = props[item.mapstate.current_tab]
                if(item.mapstate.onTabSelect){
                  tab_component_props.onTabSelect = props[
                    item.mapstate.onTabSelect
                  ].bind(this, item.template_id);
                }
              }
              if(props[item.mapstate.active_tab]){
                tab_component_props.current_tab = props[item.mapstate.active_tab]
                
              }
            }else{
              tab_component_props.defaultActiveKey = "0"
            }
            return (<Tabs
                      config={item.define}
                      extend_class={item.extend_class} 
                      key={`${from}-level-${level}-${i}`}
                      ckm={"1"}
                      {...tab_component_props}>
                <Nav variant="pills">
                  {item.define.layout.map((item_tab, index)=>{
                    if(item_tab.define.title_layout){
                      return (<Nav.Item className={"tab_item_"+index}>
                              <Nav.Link eventKey={index.toString()}>
                                 <Layout {...props} layout={item_tab.define.title_layout} level={level + 1} />
                              </Nav.Link>
                            </Nav.Item>)
                    }else{
                      return ( <Nav.Item className={"tab_item_"+index}>
                              <Nav.Link eventKey={index.toString()}>
                                {item_tab.define.title}
                              </Nav.Link>
                            </Nav.Item>)
                    }
                    
                  })}
                   
                   
                </Nav>


               <Tab.Content>

               {item.layout.map((ite, index)=>{

                return(<Tab.Pane eventKey={index.toString()} index={index.toString()} key={index.toString()}>
                    <Layout {...props} layout={ite.layout} level={level + 1} />
                </Tab.Pane>)
               })}
               </Tab.Content>
            </Tabs>)
          case "bst_items":
            return <TabsItem config={item.define}  key={`${from}-level-${level}-${i}`}/>
          case "otp_confirm":
            let otp_props = {};
            if (item.mapstate) {
              if(props[item.mapstate.otpConfig]){
                otp_props.otpConfig = props[item.mapstate.otpConfig]
              }
            }
            return <OTPConfirm className={`${item.define?.isHide ? "class-hidden" : ""}`} ref={item.ref} language={item.define.language} key={`${from}-level-${level}-${i}`} {...otp_props}/>
          case "viewcomponent":
            var props_item = {}
            if(item.mapstate){
              if(item.mapstate["viewcomponent"]){
                props_item.component_view = props[item.mapstate["viewcomponent"]]
              }
              if(item.mapstate["viewprops"]){
                props_item.viewprops = props[item.mapstate["viewprops"]]
              }
            }
            return <Component config={item.define} key={`${from}-level-${level}-${i}`} {...props_item}/>
          case "upload_frame":
            let component_props_upload = {}
            if (item.mapstate) {
              if(props[item.mapstate.layout_data]){
                  component_props_upload.data = props[item.mapstate.layout_data]
              }
              if(props[item.mapstate.onFileUpdate]){
                  component_props_upload.onFileUpdate = props[
                    item.mapstate.onFileUpdate
                  ]
                }
              }

            return (
              <div>
                <UploadFrame
                  key={`${from}-level-${level}-${i}`}
                  data={item.define.data}
                  viewOnly={item.define.viewOnly}
                  ref={item.ref}
                  col_layout={item.define.col_layout}
                  {...component_props_upload}
                />
              </div>
            );
          case "singleupload":
            var item_sgupload_props = {}
            let component_props_sgupload = {}
            if(item.properties){
              item_sgupload_props = item.properties["view_"+item.template_id]||{}
            } 
            if (item.mapstate) {
              if(props[item.mapstate.onFileUpdate]){
                  component_props_sgupload.onFileUpdate = props[
                    item.mapstate.onFileUpdate
                  ]
                }
            }
            return (
                item_sgupload_props.hide?null:<SingleUpload
                  key={`${from}-level-${level}-${i}`}
                  ref={item.ref}
                  {...component_props_sgupload}
                />
            );
          case "bank_beneficiary":
            return (
                <BeneficiaryBank extend_class={item.define.extend_class} language={item.define.language} ref={item.ref}/>
              
            );
          case "user_beneficiary":
            let component_props_bene_user = {}
            if (item.mapstate) {
              if(props[item.mapstate.data]){
                  component_props_bene_user.data = props[item.mapstate.data]
              }
              if(props[item.mapstate.onBeneChange]){
                  component_props_bene_user.onBeneChange = props[item.mapstate.onBeneChange]
              }
            }
            return (
                <BeneficiaryUser
                  ref={item.ref}
                  {...component_props_bene_user}
                />
              
            );
          default:
            return <div key={`${from}-level-${level}-${i}`}/>;
        }
      });
    }
  };

  return <React.Fragment>{renderLayout()}</React.Fragment>;
});

export default Layout;
