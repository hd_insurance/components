import React, { useEffect, useState } from "react";
const View = (props) => {
  return (
    <div>
      <div className="support-kh">
        {/* <div className="green-top-bg"/> */}
        <div className="hd-container upto-top">
          <div className="background-support">
            <h1 lng="nukfum0j">{lng.get("nukfum0j")}</h1>
            <p lng="ajyf02dw">{lng.get("ajyf02dw")}</p>
            <p lng="flob8q3d">{lng.get("flob8q3d")}</p>
            <div className="contact-info row">
              <div className="col-md-4">
                <p>
                  <i className="fas fa-phone-alt"></i> <a>1900 068 898</a>
                </p>
              </div>
              <div className="col-md-4">
                <p>
                  <i className="fas fa-envelope-open"></i>{" "}
                  <a>info@hdinsurance.com.vn</a>
                </p>
              </div>
              <div className="col-md-4">
                <p>
                  <i className="fas fa-globe-americas"></i>{" "}
                  <a>https://hdinsurance.com.vn</a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default View;
