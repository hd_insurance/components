import React, { useEffect, useState } from "react";
import FLInput from "../libs/flinput";
import FLSelect from "../libs/flinput/listselect.js";
import FLTextarea from "../libs/flinput/textarea.js";
import { Button } from 'react-bootstrap';

const View = (props) => { 

  const data = [
  	"Liên hệ trực tiếp với chúng tôi qua mail: .../ SĐT: .... hoặc điền vào form thông tin bên cạnh",
  	"Nhận lại phản hồi/ cuộc gọi từ chúng tôi "
  ]
  return (
  	<div className="register-form-dn">
  	<div className="hd-container upto">
     	<div className="row">

     	<div className="col-md-5">
     		<div className="left-info">
     			<h2>Đăng ký cho doanh nghiệp của bạn</h2>
     			{data.map((item, index)=>{
     				return (<div className="info">
			     				<i class="fas fa-check-circle"></i><p>
			     					{item}
			     				</p>
			     			</div>)
     			})}

     			

     		</div>


     	</div>


     	<div className="col-md-7">
     		<div className="dn-form">
     			<p>Để lại lời nhắn và chúng tôi sẽ liên lạc với bạn</p>
     			
     			<div className="row">
	     			<div className="col-md-6 mt-15">
			 			<FLInput
	                     	label={"Tên doanh nghiệp"}
		                    value={null}
		                    required={true}
		                  />
	     			</div>

	     			<div className="col-md-6 mt-15">
			 			<FLInput
	                     	label={"Mã số thuế"}
		                    value={null}
		                    required={true}
		                  />
	     			</div>
	     		</div>

	     		<div className="row">
	     			<div className="col-md-6 mt-15">
			 			<FLSelect
				            disable={false}
				            label={"Danh xưng"}
				            dropdown={true}
				            required={true}
				            data={[{label: "Ông (Mr.)", value:"M"}, {label: "Bà (Mrs.)", value:"F"}]}
				          />
	     			</div>

	     			<div className="col-md-6 mt-15">
			 			<FLInput
	                     	label={"Tên người liên hệ"}
		                    value={null}
		                    required={true}
		                  />
	     			</div>
	     		</div>


	     		<div className="row">
	     			<div className="col-md-6 mt-15">
			 			<FLInput
	                     	label={"Số điện thoại"}
		                    value={null}
		                    required={true}
		                  />
	     			</div>

	     			<div className="col-md-6 mt-15">
			 			<FLInput
	                     	label={"Email"}
		                    value={null}
		                    required={true}
		                  />
	     			</div>
	     		</div>


	     		<div className="row">
	     			<div className="col-md-12 mt-15">
			 			<FLTextarea
	                     	label={"Lời nhắn"}
		                    value={null}
		                    required={true}
		                  />
	     			</div>

	     		</div>


	     		<Button className="btn-submit-green mt-15">Gửi thông tin</Button>


     		</div>
     	</div>








     	</div>
    </div>

    <div className="bottom-green"/>
   </div>
  );
};

export default View;
