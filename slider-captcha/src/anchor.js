import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Card from './card';
import { SuccessIcon } from './icons';

const Anchor = ({
  onClick,
  ctcOpen,
  loading,
  text,
  fetchCaptcha,
  submitResponse,
  verified,
}) => {

  

 


  const [open, setOpen] = useState(ctcOpen);
  const [mloading, setMLoading] = useState(loading);
  const handleClose = () => { 
    setOpen(false);
  };
  const handleOpen = () => { 
    setOpen(true);
    onClick(verified);
  };
  const handleKey = (e) => {
    if (e.code === 'Enter' || e.code === 'Space') {
      setOpen(true);
    } else if (e.key === 'Escape') {
      setOpen(false);
    }
  };

  useEffect(() => {
    setOpen(ctcOpen);
  },[ctcOpen]);
  useEffect(() => {
    setMLoading(loading);
  },[loading]);

  return (
    <div>
      <div
        className="scaptcha-anchor-container scaptcha-anchor-element"
        onClick={handleOpen}
      >
       <div className="btn-captcha-event">

         
          {mloading?<div className="dot-load">
             <div class="dots dot8">
              <div class="dot"></div>
              <div class="dot"></div>
              <div class="dot"></div>
              <div class="dot"></div>
            </div>
          </div>:<div> <i class="fas fa-search"></i> {text.btn_text?text.btn_text:"Tra cứu"}</div>}
          

       </div>
      
      </div>
      {!verified && open && (
        <div>
          <div className="scaptcha-hidden" onClick={handleClose} />
          <Card
            fetchCaptcha={fetchCaptcha}
            submitResponse={submitResponse}
            text={text}
          />
        </div>
      )}
    </div>
  );
};

Anchor.propTypes = {
  onClick: PropTypes.func,
  fetchCaptcha: PropTypes.func.isRequired,
  submitResponse: PropTypes.func.isRequired,
  text: PropTypes.shape({
    btn_text: PropTypes.string,
    anchor: PropTypes.string,
    challenge: PropTypes.string,
  }).isRequired,
  verified: PropTypes.bool.isRequired,
};

export default Anchor;
