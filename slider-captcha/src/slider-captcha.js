import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Anchor from './anchor';
import Theme from './theme';

const fetchCaptcha = (create) => () =>
  (create instanceof Function)
    ? create() // Use provided promise for getting background and slider
    : fetch(create, {
        // Use create as API URL for fetch
        method: 'GET',
        credentials: 'include',
      }).then((message) => message.json());

const fetchVerification = (verify) => (response, trail) =>
  (verify instanceof Function)
    ? verify(response, trail) // Use provided promise for verifying captcha
    : fetch(verify, {
        // Verification API URL provided instead
        method: 'POST',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          response,
          trail,
        }),
      }).then((message) => message.json());

const SliderCaptcha = ({
  callback,
  onClick,
  ctcOpen,
  loading,
  create,
  verify,
  variant,
  text,
}) => {
  const [verified, setVerified] = useState(false);
  const submitResponse = (response, trail) =>
    new Promise((resolve) => {
      fetchVerification(verify)(response, trail)
        .then((verification) => {
          if (
            !verification.result ||
            verification.result !== 'success' ||
            !verification.token
          ) {
            resolve(false);
          } else {
            setTimeout(() => {
              callback(verification.token);
              setVerified(true);
            }, 500);
            resolve(true);
          }
        });
    });
  return (
    <div className="scaptcha-container">
      <Theme variant={variant} />
      <Anchor
        text={text}
        fetchCaptcha={fetchCaptcha(create)}
        submitResponse={submitResponse}
        loading={loading}
        ctcOpen={ctcOpen}
        verified={verified}
        onClick={onClick}
      />
    </div>
  );
};

SliderCaptcha.propTypes = {
  callback: PropTypes.func,
  onClick: PropTypes.func,
  ctcOpen: PropTypes.boolean,
  loading: PropTypes.boolean,
  create: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  verify: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
  variant: PropTypes.string,
  text: PropTypes.shape({
    btn_text: PropTypes.string,
    anchor: PropTypes.string,
    challenge: PropTypes.string,
  }),
};

SliderCaptcha.defaultProps = {
  callback: (token) => console.log(token), // eslint-disable-line no-console
  onClick: ()=>{},
  ctcOpen: false,
  loading: false,
  create: 'captcha/create',
  verify: 'captcha/verify',
  variant: 'light',
  text: {
    btn_text: 'TC',
    anchor: 'I am human',
    challenge: 'Slide to finish the puzzle',
  },
};

export default SliderCaptcha;
