import React, {useEffect, useState, createRef} from "react";
import Link from 'next/link'
import currencyFormatter from 'currency-formatter';
import {Button, Table, Modal, Row, Form} from 'react-bootstrap';
// import styles from "../../sdk/src/css/style.module.css";
import {FLInput} from "hdi-uikit";
import api from "../../services/Network";


const View = (props) => {

    const formRef = createRef();
    const [claimInfo, setClaimInfo] = useState(null);
    const [certInfo, setCertInfo] = useState({});
    const [enableLink, setEnableLink] = useState(false);

    const [validated, setValidated] = useState(false);

    const [showModal, setShowModal] = useState(false);
    const [showSuccess, setShowSuccess] = useState(false);
    const [mode, setMode] = useState("0");
    const [name, setName] = useState(null);
    const [mst, setMst] = useState(null);
    const [email, setEmail] = useState(null);
    const [address, setAddress] = useState(null);

    const [loadingBtn, setLoadingBtn] = useState(false);

    useEffect(() => {
        if(props.searchcode === "BAY_AT"){
            const timer = setTimeout(() => {
                setEnableLink(true)
            }, 8000);
            return () => clearTimeout(timer);
        }else{
            setEnableLink(true);
        }
    }, []);

    useEffect(() => {
        setCertInfo(props.certInfo)
    }, [props.certInfo]);

    useEffect(() => {
        if(props.searchcode === 'LOST_BAGGAGE' &&  props.claimInfo && props.claimInfo.length > 0) {
            let _claimTemp = props.claimInfo.filter(p => p.CERTIFICATE_NO === props?.certInfo.CERTIFICATE_NO);
            if(_claimTemp.length > 0){
                setClaimInfo(_claimTemp)
            } else {
                setClaimInfo(null);
            }
        } else {
            setClaimInfo(props.claimInfo)
        }
    }, [props.claimInfo]);


    const onRdChange = (e) => {
        if (e.target.checked) {
            setMode(e.target.value);
        }
    };

    const showModalBillingRequest = () =>{
        setName(null);
        setMst(null);
        setEmail(null);
        setAddress(null);
        setShowModal(true);
    }

    const redirectInvoiSearch = () =>{
        window.open("https://einvoice.fast.com.vn/index.aspx", "_blank")
    }

    const submitForm = async () =>{
        const form = formRef.current;
        setLoadingBtn(true);
        if (form.checkValidity() === false) {
            setValidated(true);
            setLoadingBtn(false);
            return false;
        }
        const param = {
            detail_code:  certInfo.DETAIL_CODE,
            taxcode: mode === "1" ?  mst : null,
            email: email,
            address:  mode === "1" ? address : null,
            e_name:  mode === "1" ? name : null,
            type: mode === "0" ? 'CN' : 'CQ',
        };
        try{
            const response = await api.post(
                `/api/vj/billing-request`,
                param
            );
            if(response.success){
                setLoadingBtn(false);
                setShowModal(false);
                setShowSuccess(true);
            }else{
                setLoadingBtn(false);
                alert('Có lỗi xảy ra. Vui lòng thử lại!')
            }
        }catch(e){
            setLoadingBtn(false);
            alert('Có lỗi xảy ra. Vui lòng thử lại!')
        }
    }

    return (
        <div className="cardholder">
            <div className="hd-container">

                <h1 lng={"gihxcq1u"}>{lng.get("gihxcq1u")}</h1>
                <div className="cardholder-info">
                    <div className="card-head">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="userinfo">
                                    <img src="/img/hdi-small.svg" className="icon-hdi"/>
                                    <div className="k">
                                        <div className="k1" lng="2pohwwwd">{lng.get("2pohwwwd")}:</div>
                                        <div className="k2">{certInfo.CERTIFICATE_NO}</div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-6">
                                {/* <div className="right-col">
                <p>Nhận thông tin trực tiếp và nhanh chóng về bảo hiểm, vui lòng bổ sung thông tin liên hệ</p>
                <Button className="btn-style2 xa2d">Bổ sung thông tin</Button>
              </div> */}

                            </div>
                        </div>
                    </div>

                    <div className="card-content">
                        <div className="row">

                            <div className="col-md-4">
                                <div className="data-preview">
                                    <div className="r">
                                        <div className="n" lng="uzqdqnyu">{lng.get("uzqdqnyu")}:</div>
                                        <div className="v">{certInfo.PRODUCT_NAME}</div>
                                    </div>
                                    {/* <div className="r">
                    <div className="n">Trạng thái HĐ: </div>
                    <div className="v">{certInfo.STATUS}</div>
                </div> */}
                                    <div className="r">
                                        <div className="n" lng="gcvppmdy">{lng.get("gcvppmdy")}:</div>
                                        <div className="v">{certInfo.BOOKING_ID}</div>
                                    </div>
                                    <div className="r">
                                        <div className="n">{lng.get("jcqxauhp")}:</div>
                                        <div className="v">{certInfo.FLI_NO}</div>
                                    </div>
                                    <div className="r">
                                        <div className="n" lng="fdjhx4bj">{lng.get("fdjhx4bj")}:</div>
                                        <div className="v">{certInfo.DEP_NAME} - {certInfo.ARR_NAME}</div>
                                    </div>

                                </div>
                            </div>

                            <div className="col-md-4 bdr-right2">
                                <div className="data-preview">
                                    {/* <div className="r">
                        <div className="n">Thời hạn bảo hiểm:</div>
                        <div className="v">--</div>
                    </div> */}
                                    <div className="r">
                                        <div className="n">{lng.get("8jrol4t4")}:</div>
                                        <div className="v">{certInfo.NAME}</div>
                                    </div>
                                    <div className="r">
                                        <div className="n" lng="5yjtbbp1">{lng.get("5yjtbbp1")}:</div>
                                        <div className="v">{certInfo.FLI_R_DATE}</div>
                                    </div>
                                    <div className="r">
                                        <div className="n" lng="90nhfruf">{lng.get("90nhfruf")}:</div>
                                        <div className="v">{certInfo.SEAT}</div>
                                    </div>

                                </div>
                            </div>


                            <div className="col-md-4 ">

                                <div className="btn-inf-group">
                                    {enableLink ?
                                        <a href={`${process.env.NEXT_PUBLIC_APISERVICE}/f/${certInfo.FILE_SIGNED}`}
                                           target="_blank"><Button className="btn-style1 w100"><i
                                            class="far fa-eye"></i>{lng.get("zlxsk2ka")}</Button></a> :
                                        <Button className="btn-style1 w100"><i
                                            class="far fa-eye"></i>{lng.get("zlxsk2ka")}</Button>}
                                    {(claimInfo == null) && <a href={`/claim-form?id=${certInfo.DETAIL_CODE}&prod=${certInfo.PRODUCT_CODE}`}>
                                        <Button className="btn-style1 w100 bx1" hidden={certInfo.IS_CLAIM === '0'}><i
                                            class="fas fa-plus-circle"></i> {lng.get("1pzrys4k")}</Button>
                                    </a>}
                                    {certInfo.IS_BILL === "1" && (
                                        <label className={"request-invoice"} onClick={showModalBillingRequest}>
                                            {lng.get("tqb0ktct")}
                                        </label>
                                    )}
                                    {certInfo.IS_BILL === "2" && (
                                        <label className={"request-invoice"} onClick={redirectInvoiSearch}>
                                            {lng.get("srlhuvic")}
                                        </label>
                                    )}
                                </div>

                            </div>

                        </div>
                        <Modal
                            show={showModal}
                            onHide={() =>setShowModal(false)}
                            backdrop="static"
                            keyboard={false}
                        >
                            <Modal.Header closeButton>
                                <Modal.Title>{lng.get("tqb0ktct")}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body style={{fontSize: 14}}>
                                <label>{lng.get("c558ju30")}</label>
                                <Form
                                    ref={formRef}
                                    className="search-form-input"
                                    noValidate
                                    validated={validated}
                                >

                                    <div  style={{display: "flex"}}>
                                        <label>{lng.get("3souckhw")}</label>

                                        <div className="rd_group" style={{marginLeft: 24}}>
                                            <input
                                                className="radio"
                                                type="radio"
                                                name="rd1"
                                                value={"0"}
                                                onChange={(e) => onRdChange(e)}
                                                id="radio-1"
                                                defaultChecked={mode === "0"}
                                            />
                                            <label htmlFor="radio-1" style={{fontSize: 14}}>{lng.get("ygwol2hm")}</label>
                                            <div className="sp-hoz"/>
                                            <input
                                                className="radio"
                                                type="radio"
                                                name="rd1"
                                                value={1}
                                                onChange={(e) => onRdChange(e)}
                                                id="radio-2"
                                                defaultChecked={mode === "1"}
                                                style={{fontSize: 14}}
                                            />
                                            <label htmlFor="radio-2" style={{fontSize: 14}}>{lng.get("pbkrvb3t")}</label>
                                        </div>
                                    </div>

                                    {mode === "1" ? (
                                        <>
                                            <div className={"col-12 mt-15"}>
                                                <FLInput
                                                    disable={false}
                                                    value={name}
                                                    changeEvent={setName}
                                                    label={lng.get("tqqui0ui")}
                                                    required={true}
                                                />
                                            </div>
                                            <div className={"col-12 mt-15"}>
                                                <FLInput
                                                    disable={false}
                                                    value={mst}
                                                    changeEvent={setMst}
                                                    label={lng.get("doqsxnvv")}
                                                    required={true}
                                                />
                                            </div>
                                            <div className={"col-12 mt-15"}>
                                                <FLInput
                                                    disable={false}
                                                    value={email}
                                                    changeEvent={setEmail}
                                                    label={lng.get("sfz8vnks")}
                                                    required={true}
                                                />
                                            </div>
                                            <div className={"col-12 mt-15"}>
                                                <FLInput
                                                    disable={false}
                                                    value={address}
                                                    changeEvent={setAddress}
                                                    label={lng.get("4salp6p0")}
                                                    required={true}
                                                />
                                            </div>
                                        </>
                                    ): (
                                        <div className={"col-12 mt-15"}>
                                            <FLInput
                                                disable={false}
                                                value={email}
                                                changeEvent={setEmail}
                                                label={lng.get("sfz8vnks")}
                                                required={true}
                                            />
                                        </div>
                                    )}
                                </Form>
                                <div className={"footer-modal-request"}>
                                    <Button className="btn-request-invoice"
                                            style={{margin: 'auto'}}
                                            type="submit"
                                            onClick={() =>submitForm()}
                                            disabled={loadingBtn}
                                    >
                                        <div lng="rducp5x2">{lng.get('tqb0ktct')}</div>
                                    </Button>
                                    <div style={{marginTop: 16}}>{lng.get("yiinsih7")} <label className="request-hotline">1900 068898</label></div>
                                </div>
                            </Modal.Body>
                        </Modal>


                        <Modal
                            show={showSuccess}
                            onHide={() =>setShowSuccess(false)}
                        >
                            <Modal.Body style={{fontSize: 14, textAlign: 'center'}}>

                                <h3>{lng.get("s88aukdl")}</h3>
                                <div className={"footer-modal-request"}>
                                    <Button className="btn-request-invoice"
                                            style={{margin: 'auto', width: 125}}
                                            onClick={() =>setShowSuccess(false)}
                                            disabled={loadingBtn}
                                    >
                                        <div lng="rducp5x2">{lng.get('24m1ysco')}</div>
                                    </Button>
                                </div>


                            </Modal.Body>
                        </Modal>

                        {claimInfo && <Table striped bordered hover size="sm" className="mt-15 data-table-claim">
                            <thead>
                            <tr>
                                <th>{lng.get("wbucetku")}</th>
                                <th>{lng.get("5sxi0rfk")}</th>
                                <th>{lng.get("zyonhh3t")}</th>
                                <th>{lng.get("qyb8rbnd")}</th>
                                <th>{lng.get("wmxszedf")}</th>
                                <th>{lng.get("dbshfxcg")}</th>
                                <th>{lng.get("qdizh5gh")}</th>
                            </tr>
                            </thead>

                            <tbody>
                            {claimInfo.map((claim, index) => {
                                return (<tr className="isr-claim-row">
                                    <td>{index + 1}</td>
                                    <td>{claim.NAME}</td>
                                    <td>{claim.CLAIM_DATE}</td>
                                    <td>{lng.getTextFromCode(claim.STATUS)}</td>
                                    <td>{currencyFormatter.format(claim.REQUIRED_AMOUNT, {
                                        code: l.g("bhsk.currency"),
                                        precision: 0,
                                        format: '%v %s',
                                        symbol: l.g("bhsk.currency")
                                    })}</td>
                                    <td>{claim.AMOUNT ? currencyFormatter.format(claim.AMOUNT, {
                                        code: l.g("bhsk.currency"),
                                        precision: 0,
                                        format: '%v %s',
                                        symbol: l.g("bhsk.currency")
                                    }) : "--"}</td>
                                    <td>{claim.PAY_DATE}</td>
                                </tr>)
                            })}


                            </tbody>
                        </Table>
                        }


                    </div>


                </div>
            </div>
        </div>
    );
};

export default View;
