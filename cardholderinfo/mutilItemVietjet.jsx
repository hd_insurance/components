import React, {useEffect, useState, createRef} from "react";
import Link from 'next/link'
import currencyFormatter from 'currency-formatter';
import {Button, Table, Modal, Row, Form} from 'react-bootstrap';
import styles from "sdk-hdi/src/css/style.module.css";
import {FLInput} from "hdi-uikit";
import api from "../../services/Network";


const View = (props) => {
    const [listInfo, setListInfo] = useState(props.listCertInfo);
    useEffect(() =>{
        setListInfo(props.listCertInfo);
    },[props.listCertInfo])

    const viewInfo = (i) =>{
        props.previewGCN(i)
    }
    return (
        <>
            {listInfo.map((vl, i) =>{
                return(
                    <div className="cardholder-info" style={{position: "relative", marginTop: 12}}>
                        <div className="card-head">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="userinfo">
                                        <img src="/img/hdi-small.svg" className="icon-hdi"/>
                                        <div className="k">
                                            <div className="k1" lng="2pohwwwd">{lng.get("2pohwwwd")}:</div>
                                            <div className="k2">{vl.CERTIFICATE_NO}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="card-content">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="data-preview">
                                        <div className="r">
                                            <div className="n" lng="uzqdqnyu">{lng.get("uzqdqnyu")}:</div>
                                            <div className="v">{vl.PRODUCT_NAME}</div>
                                        </div>
                                        <div className="r">
                                            <div className="n" lng="gcvppmdy">{lng.get("gcvppmdy")}:</div>
                                            <div className="v">{vl.BOOKING_ID}</div>
                                        </div>
                                        <div className="r">
                                            <div className="n">{lng.get("jcqxauhp")}:</div>
                                            <div className="v">{vl.FLI_NO}</div>
                                        </div>
                                        <div className="r">
                                            <div className="n" lng="fdjhx4bj">{lng.get("fdjhx4bj")}:</div>
                                            <div className="v">{vl.DEP_NAME} - {vl.ARR_NAME}</div>
                                        </div>
                                        <div className="r">
                                            <div className="n">{lng.get("8jrol4t4")}:</div>
                                            <div className="v">{vl.NAME}</div>
                                        </div>
                                        <div className="r">
                                            <div className="n" lng="5yjtbbp1">{lng.get("5yjtbbp1")}:</div>
                                            <div className="v">{vl.FLI_R_DATE}</div>
                                        </div>
                                        <div className="r">
                                            <div className="n" lng="90nhfruf">{lng.get("90nhfruf")}:</div>
                                            <div className="v">{vl.SEAT}</div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="choose-item">
                            <div className='btn-choose-item' onClick={() =>viewInfo(i)}>
                                Xem giấy chứng nhận
                            </div>
                        </div>

                    </div>
                )
            })}
        </>
    );
};

export default View;
