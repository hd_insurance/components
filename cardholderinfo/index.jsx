import React, { useEffect, useState, createRef } from "react";
import { Button, Modal } from "react-bootstrap";

import Popover from "react-popover";

const QRCode = require("qrcode.react");
const _ = require("lodash");

const View = (props) => {
  const [infor, setInfor] = useState(props.cardOwner);
  const [openBenefit, setOpenBenefit] = useState(false);
  const [isShow, setShow] = useState(false);
  const cardholderRef = createRef();
  useEffect(() => {
    if (!_.isEmpty(props.cardOwner)) {
      setInfor({
        ...props.cardOwner,
        DESCRIPTION: props.cardOwner.DESCRIPTION
          ? JSON.parse(props.cardOwner.DESCRIPTION).find(
              (e) => e.PACK_CODE == props.cardOwner.PACK_CODE
            )
          : null,
      });
      window.scrollTo({
        top: cardholderRef.current.offsetTop,
        left: 100,
        behavior: "smooth",
      });
    }
  }, [props.cardOwner]);
  const viewGCN = () => {
    window.open(infor.URL_CERTIFICATE, "_blank").focus();
  };

  const actionopenBenefit = () => {
    if (infor.DESCRIPTION?.BENEFITS.length > 0) {
      setOpenBenefit(true);
    }
  };
  const popoverBenefit = {
    isOpen: openBenefit,
    place: "below",
    preferPlace: "right",
    onOuterAction: () => setOpenBenefit(false),
    body: [
      <div className="list-benefit-relative">
        <div className="h">
          {!_.isEmpty(infor) ? infor.DESCRIPTION?.PACK_NAME : null}
        </div>
        <div className="list-xxx">
          {(!_.isEmpty(infor) && infor.DESCRIPTION?.BENEFITS.length > 0
            ? infor.DESCRIPTION.BENEFITS
            : []
          ).map((item, index) => {
            return (
              <div key={index} className="ite">
                {item.NAME}
              </div>
            );
          })}
        </div>
      </div>,
    ],
  };

  if (_.isEmpty(props.cardOwner)) {
    return <div></div>;
  } else {
    return (
      <div className="cardholder" ref={cardholderRef}>
        <Modal
          show={isShow}
          onHide={() => setShow(false)}
          dialogClassName="modal-popup-visa-ycbt"
        >
          <Modal.Header closeButton>
            <Modal.Title>{lng.get("qbsprofn")}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="row">
              <div className="col-12">
                <div className="yc-success" lng="wxrcgp88">
                  {lng.get("wxrcgp88")}
                </div>
              </div>
            </div>
            <div className="row mt-15">
              <div className="col-12">
                <div lng="1shxqzxa ceelajtn">
                  {lng.get("1shxqzxa")}{" "}
                  <a href="tel:1900 969 712">
                    <b className="hotline_visa">1900 636 730</b>
                  </a>
                  {lng.get("ceelajtn")}
                </div>
              </div>
            </div>
            <div className="row justify-content-center">
              <div className="col-md-5 mt-15">
                <button
                  onClick={() => setShow(false)}
                  type="button"
                  className="btn btn-primary button-close-popup-visa-ycbt"
                  lng="bbn5ylyq"
                >
                  {lng.get("bbn5ylyq")}
                </button>
              </div>
            </div>
          </Modal.Body>
        </Modal>
        <div className="hd-container">
          <h1 lng="i0usd0ri" className="uppercase-first-letter">
            {lng.get("i0usd0ri")}
          </h1>
          <div className="cardholder-info">
            <div className="card-head">
              <div className="row">
                <div className="col-md-5">
                  <div className="userinfo">
                    <img src="/img/hdi-small.svg" className="icon-hdi" />
                    <div className="k">
                      <div className="k1" lng="kcsbk9vq">
                        {lng.get("kcsbk9vq")}
                      </div>
                      <div className="k2">{infor.NAME}</div>
                    </div>
                  </div>
                </div>

                <div className="col-md-7">
                  {/* <div className="right-col">
                  <p>Hợp đồng của bạn đã đến hạn thanh toán phí. Bạn còn 15 ngày để thanh toán phí và tiếp tục duy trì hợp đồng bảo hiểm.</p>
                  <Button className="btn-style2 xa2d">Thanh toán phí</Button>
                </div> */}
                </div>
              </div>
            </div>

            <div className="card-content">
              <div className="row">
                <div className="col-md-5 bdr-right">
                  <div className="data-preview">
                    <div className="r">
                      <div className="n" lng="azzhdnof">
                        {lng.get("azzhdnof")}
                      </div>
                      <div className="v">{infor.CERTIFICATE_NO}</div>
                    </div>
                    <div className="r">
                      <div className="n" lng="xhdirsq1">
                        {lng.get("xhdirsq1")}
                      </div>
                      <div className="v">{infor.PRODUCT_NAME}</div>
                    </div>
                    <div className="r">
                      <div className="n" lng="hklly7fi">
                        {lng.get("hklly7fi")}
                      </div>
                      <div className="v">{`${infor.EFFECTIVE_DATE} - ${infor.EXPIRATION_DATE}`}</div>
                    </div>
                    <div className="r">
                      <div className="n" lng="uheyw3ed">
                        {lng.get("uheyw3ed")}
                      </div>
                      <div className="v">
                        <Popover {...popoverBenefit}>
                          <div
                            lng="va2qpeiv"
                            className="show-detail-ql-visa"
                            onClick={(e) => {
                              actionopenBenefit();
                            }}
                          >
                            {lng.get("va2qpeiv")}
                          </div>
                        </Popover>
                      </div>
                    </div>
                    <div className="r">
                      <div className="n" lng="he5cmjow">
                        {lng.get("he5cmjow")}
                      </div>
                      <div className="v">
                        <a href="tel:1900 636 730">
                          <b className="hotline_visa">1900 636 730</b>
                        </a>
                      </div>
                    </div>
                    <div className="r">
                      <div className="n" lng="o9kxlypn">
                        {lng.get("o9kxlypn")}
                      </div>
                      <div className="v">
                        <a href="tel:1900 969 712">
                          <b className="hotline_visa">1900 969 612</b>
                        </a>
                      </div>
                    </div>
                    <div className="r">
                      <div className="n" lng="doctor">
                        Hotline Doctor Anywhere:
                      </div>
                      <div className="v">
                        <a href="tel:1900 969 712">
                          <b className="hotline_visa">1900 2819</b>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-7">
                  <div className="row">
                    <div className="col-md-6">
                      <div className="rq-inf">
                        {/* <img src="/img/websiteQRCode_noFrame.png" /> */}
                        <QRCode
                          id="qrcode"
                          value={
                            infor.URL_CERTIFICATE ? infor.URL_CERTIFICATE : ""
                          }
                          size={160}
                          level={"M"}
                          includeMargin={true}
                        />
                        <p lng="bf4gof86">{lng.get("bf4gof86")}</p>
                      </div>
                    </div>

                    <div className="col-md-6">
                      <div className="btn-inf-group d-flex flex-column align-items-center">
                        <Button
                          onClick={viewGCN}
                          className="btn-style1 w100"
                          lng="adfqb50i"
                        >
                          <i className="far fa-eye"></i> {lng.get("adfqb50i")}
                        </Button>
                        {/* <Button
                          className="btn-style1 w100 bx1"
                          lng="dxahmeau"
                          onClick={() => setShow(true)}
                        >
                          <i className="fas fa-plus-circle"></i>{" "}
                          {lng.get("dxahmeau")}
                        </Button> */}
                        <div
                          onClick={() => props.scrollToBT(3)}
                          className="hotline_visa"
                        >
                          <b>{lng.get("6vuapatp")}</b>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default View;
