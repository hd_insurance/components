import Link from "next/link";

import React, { useEffect, useState } from "react";
import {
  Button,
  ButtonGroup,
  Dropdown,
  DropdownButton,
  Form,
} from "react-bootstrap";
import { slide as Menu } from "react-burger-menu";
import api from "../services/Network.js";

const Header = (props) => {
  const config = props.config || {};
  const [lang, setLang] = React.useState(l.getLang() || "vi");
  const [isOpen, setOpen] = React.useState(false);
  var isMenuOpen = function (state) {
    setOpen(state.isOpen);
    return state.isOpen;
  };

  const onLangSelect = async (lng) => {
    setLang(lng);
    l.setLang(lng);
    
    const result = await api.get(`/landing-api/current-language/${lng}`)
    window.location.reload(false);
  };

  useEffect(() =>{
    setLang(l.getLang())
  },[l.getLang()])

  return (
    <nav className="navbar navbar-expand-lg navbar-light-hdi bg-light-hdi">
      <a className="navbar-brand" href="#">
        <img src="/img/logo-hdi.svg" alt="HDI" />
        {/*<img className="logo-vj" src="/img/logo-vietjet.png" alt="VJ" />*/}
      </a>

      {config.contact && (
        <div className="webinfo mobile">
          <p>
            <i className="fas fa-phone-alt"></i>
            {config.contact.phone}
          </p>
          <p>
            <i className="fas fa-envelope"></i>
            {config.contact.email}
          </p>
        </div>
      )}

      <button
        className="navbar-toggler"
        onClick={() => setOpen(true)}
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon">
          <i className="fas fa-bars"></i>
        </span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <div className="title-header-placholder">
          {config.contact && (
            <div className="webinfo">
              <p>
                <a href={`tel:${config.contact.phone}`}>
                  <i className="fas fa-phone-alt"></i>
                  {config.contact.phone}
                </a>
              </p>
              <p>
                <a href={`mailto:${config.contact.email}`}>
                  <i className="fas fa-envelope"></i>
                  {config.contact.email}
                </a>
              </p>
            </div>
          )}
        </div>
        <div className="header-r-menu my-2 my-lg-0">
          <DropdownButton
            as={ButtonGroup}
            title={
              <span style={{ paddingRight: 5, color: '#329945'}}>
                <i className="fas fa-globe-asia"></i>
                {l.getLang() == "vi"
                  ? l.g("header.langtitle.vi")
                  : l.g("header.langtitle.en")}
              </span>
            }
            className="btn-dropdown-lang"
          >
            <Dropdown.Item
              as="button"
              onSelect={() => onLangSelect("vi")}
              active={lang == "vi"}
              className="drop-item-mn"
            >
              <img src="/img/vi-flag.svg" />
              <div className="text">{l.g("header.langtitle.vi")}</div>{" "}
              {lang == "vi" ? <i className="fas fa-check-circle"></i> : ""}
            </Dropdown.Item>
            <Dropdown.Divider />
            {props.org?.toLowerCase() === 'vietjet_vn' && (
                <Dropdown.Item
                    as="button"
                    onSelect={() => onLangSelect("en")}
                    active={lang == "en"}
                    className="drop-item-mn"
                >
                  <img src="/img/en-flag.svg" />
                  <div className="text">{l.g("header.langtitle.en")}</div>{" "}
                  {lang == "en" ? <i className="fas fa-check-circle"></i> : ""}
                </Dropdown.Item>
            )}
          </DropdownButton>

          {/* <DropdownButton
            as={ButtonGroup}
            title={
              <span style={{ paddingRight: 5 }}>
                <i className="far fa-question-circle"></i>{l.g('header.help.title')}
              </span>
            }
            className="btn-dropdown-lang"
          >
            <Dropdown.Item href="#/action-1" className="drop-item-mn">
              <img src="/img/hdsd-ic.svg" />
              <div className="text">{l.g('header.help.menu.item1')}</div>{" "}
              <i className="fas fa-chevron-right"></i>
            </Dropdown.Item>
            <Dropdown.Divider />
            <Dropdown.Item href="#/action-2" className="drop-item-mn">
              <img src="/img/dieukhoan-ic.svg" />
              <div className="text">{l.g('header.help.menu.item2')}</div>
              <i className="fas fa-chevron-right"></i>
            </Dropdown.Item>
            <Dropdown.Divider />
            <Dropdown.Item href="#/action-3" className="drop-item-mn">
              <img src="/img/lienhe-ic.svg" />
              <div className="text">{l.g('header.help.menu.item3')}</div>
              <i className="fas fa-chevron-right"></i>
            </Dropdown.Item>
          </DropdownButton>
        */}
        </div>
      </div>

      <Menu
        isOpen={isOpen}
        width={280}
        right
        customBurgerIcon={false}
        customCrossIcon={false}
        onStateChange={isMenuOpen}
      >
        <div className="lg-mobile">
          <DropdownButton
            as={ButtonGroup}
            title={
              <span style={{ paddingRight: 5 }}>
                <i className="fas fa-globe-asia"></i>
                {l.getLang() == "vi"
                    ? l.g("header.langtitle.vi")
                    : l.g("header.langtitle.en")}
              </span>
            }
            className="btn-dropdown-lang"
          >
            <Dropdown.Item
              as="button"
              onSelect={() => onLangSelect("vi")}
              active={lang == "vi"}
              className="drop-item-mn"
            >
              <img src="/img/vi-flag.svg" />
              <div className="text">{l.g("header.langtitle.vi")}</div>
              {lang == "vi" ? <i className="fas fa-check-circle"></i> : ""}
            </Dropdown.Item>
            <Dropdown.Divider />
            {props.org?.toLowerCase() === 'vietjet_vn' && (
                <Dropdown.Item
                    as="button"
                    onSelect={() => onLangSelect("en")}
                    active={lang == "en"}
                    className="drop-item-mn"
                >
                  <img src="/img/en-flag.svg" />
                  <div className="text">{l.g("header.langtitle.en")}</div>{" "}
                  {lang == "en" ? <i className="fas fa-check-circle"></i> : ""}
                </Dropdown.Item>
            )}
          </DropdownButton>
          <div className="btn-close-menu" onClick={() => setOpen(false)}>
            <i className="fas fa-times"></i>
          </div>
        </div>
        <a id="home" className="menu-item" href="/">
          <img src="/img/hdsd-ic.svg" />
          {l.g("header.help.menu.item1")}
        </a>
        <a id="about" className="menu-item" href="/">
          <img src="/img/dieukhoan-ic.svg" />
          {l.g("header.help.menu.item2")}
        </a>
        <a id="contact" className="menu-item" href="/">
          <img src="/img/lienhe-ic.svg" />
          {l.g("header.help.menu.item3")}
        </a>
      </Menu>
    </nav>
  );
};
export default Header;
